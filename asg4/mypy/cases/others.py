#!/usr/bin/env python

x = 0b111

print x

y = 0B1010

print y

x=010
print x

print 0o10
print 0O10

print 0xff
print 0XFF

print 010 * 12

print 0B10 * 12

print 0b10 * 12

print 0XFF * 12

print 0xff * 12


