#include "node.h"
#include "poolOfNodes.h"
class Literal : public Node {
public:
  virtual ~Literal() {}

  virtual const Literal* operator+(const Literal& rhs) const =0;
  virtual const Literal* opPlus(double) const =0;
  virtual const Literal* opPlus(int) const =0;

  virtual const Literal* operator*(const Literal& rhs) const =0;
  virtual const Literal* opMult(double) const =0;
  virtual const Literal* opMult(int) const =0;

  virtual const Literal* operator-(const Literal& rhs) const =0;
  virtual const Literal* opSubt(double) const =0;
  virtual const Literal* opSubt(int) const =0;

  virtual const Literal* operator/(const Literal& rhs) const =0;
  virtual const Literal* opDiv(double) const =0;
  virtual const Literal* opDiv(int) const =0;

  virtual const Literal* operator|(const Literal& rhs) const =0;
  virtual const Literal* opBar(double) const =0;
  virtual const Literal* opBar(int) const =0;

  virtual const Literal* operator^(const Literal& rhs) const =0;
  virtual const Literal* opCircumflex(double) const =0;
  virtual const Literal* opCircumflex(int) const =0;

  virtual const Literal* operator&(const Literal& rhs) const =0;
  virtual const Literal* opAmpersand(double) const =0;
  virtual const Literal* opAmpersand(int) const =0;

  virtual const Literal* operator<<(const Literal& rhs) const =0;
  virtual const Literal* opLeftShift(double) const =0;
  virtual const Literal* opLeftShift(int) const =0;

  virtual const Literal* operator>>(const Literal& rhs) const =0;
  virtual const Literal* opRightShift(double) const =0;
  virtual const Literal* opRightShift(int) const =0;

  virtual const Literal* operator%(const Literal& rhs) const =0;
  virtual const Literal* opMod(double) const = 0;
  virtual const Literal* opMod(int) const = 0;
  
  virtual const Literal* floorDiv(const Literal& rhs) const = 0;
  virtual const Literal* opFloorDiv(double) const = 0;
  virtual const Literal* opFloorDiv(int) const = 0;

  virtual const Literal* powerMul(const Literal& rhs) const = 0;
  virtual const Literal* opPowerMul(double) const = 0;
  virtual const Literal* opPowerMul(int) const = 0;

  virtual const Literal* eval() const = 0;
  virtual void print() const { 
    std::cout << "No Way" << std::endl; 
  }

  friend Literal* floorDiv(Literal* left , Literal* right);
  friend Literal* powerMul(Literal* left , Literal* right);
};

class FloatLiteral: public Literal {
public:
  FloatLiteral(double _val): val(_val) {}

  virtual const Literal* operator+(const Literal& rhs) const  {
    return rhs.opPlus(val);
  }
  virtual const Literal* opPlus(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs + val);
    PoolOfNodes::getInstance().add(node);
    return node; 
  }
  virtual const Literal* opPlus(int lhs) const  {
    const Literal* node = new FloatLiteral(lhs + val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

  virtual const Literal* operator-(const Literal& rhs) const  {
    return rhs.opSubt(val);
  }
  virtual const Literal* opSubt(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
  virtual const Literal* opSubt(int lhs) const  {
    const Literal* node = new FloatLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

  virtual const Literal* operator*(const Literal& rhs) const  {
    return rhs.opMult(val);
  }
  virtual const Literal* opMult(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
  virtual const Literal* opMult(int lhs) const  {
    const Literal* node = new FloatLiteral(static_cast<double>(lhs) * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

  virtual const Literal* operator/(const Literal& rhs) const  {
    return rhs.opDiv(val);
  }
  virtual const Literal* opDiv(double lhs) const  {
    if ( val == 0 ) throw std::string("Zero Division Error");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
  virtual const Literal* opDiv(int lhs) const  {
    if ( val == 0 ) throw std::string("Zero Division Error");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

  virtual const Literal* eval() const { return this; }
  virtual void print() const { 
    std::cout << "FLOAT: " << std::setprecision(17) << val << std::endl;
  }

  virtual const Literal* operator|(const Literal& rhs) const {
	return rhs.opBar(val);
  }
  virtual const Literal* opBar(double) const {
	throw std::string("Bar operation : unsupported type for float and float");
  }
  virtual const Literal* opBar(int) const {
	throw std::string("Bar operation : unsupported type for float and int");
  }

  virtual const Literal* operator^(const Literal& rhs) const {
	return rhs.opCircumflex(val);
  }
  virtual const Literal* opCircumflex(double) const {
	throw std::string("Circumflex operation : unsupported type for float and float");
  }
  virtual const Literal* opCircumflex(int) const {
	throw std::string("Circumflex opeartion : unsupported type for float and int");
  }

  virtual const Literal* operator&(const Literal& rhs) const {
	return rhs.opAmpersand(val);
  }
  virtual const Literal* opAmpersand(double) const {
	throw std::string("Ampersand opeartion : unsupported type for float and float");
  }
  virtual const Literal* opAmpersand(int) const {
	throw std::string("Ampersand opeartion : unsupported type for flaot and int");
  }

  virtual const Literal* operator<<(const Literal& rhs) const {
	return rhs.opLeftShift(val);
  }
  virtual const Literal* opLeftShift(double) const {
	throw std::string("Leftshift operation : unsupported type for float and float");
  }
  virtual const Literal* opLeftShift(int) const {
	throw std::string("Leftshift operation : unsupported type for float and int");
  }

  virtual const Literal* operator>>(const Literal& rhs) const {
	return rhs.opRightShift(val);
  }
  virtual const Literal* opRightShift(double) const {
	throw std::string("Rightshift operation : unsupported type for float and float");
  }
  virtual const Literal* opRightShift(int) const {
	throw std::string("Leftshift operation : unsupported type for float and int");
  }

  virtual const Literal* operator%(const Literal& rhs) const {
	return rhs.opMod(val);
  }
  virtual const Literal* opMod(double lhs) const {
	if (val > -PRECISION && val < PRECISION) {
	  throw std::string("Mode operation : zero division error");
	}
	const Literal* node = new FloatLiteral(lhs - val *  floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  virtual const Literal* opMod(int lhs) const {
	if (val > -PRECISION && val < PRECISION) {
	  throw std::string("Mode operation : zero divison error");
	}

	const Literal* node = new FloatLiteral(lhs - val * floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }

  virtual const Literal* floorDiv(const Literal& rhs) const {
	return rhs.opFloorDiv(val);
  }
  virtual const Literal* opFloorDiv(double lhs) const {
  if (val > -PRECISION && val < PRECISION) {
		throw std::string("Operation // : zero division error");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  virtual const Literal* opFloorDiv(int lhs) const {
    if (val > -PRECISION && val < PRECISION) {
		throw std::string("Operation // : zero division error");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }

  virtual const Literal* powerMul(const Literal& rhs) const {
	return rhs.opPowerMul(val);
  }
  virtual const Literal* opPowerMul(double lhs) const {
    try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw std::string("Operation ** : error");
	}
  }
  virtual const Literal* opPowerMul(int lhs) const {
	try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw std::string("Operation ** : error");
    }	
  }


private:
  double val;
};

class IntLiteral: public Literal {
public:
 IntLiteral(int _val): val(_val) {}

  virtual const Literal* operator+(const Literal& rhs) const  {
    return rhs.opPlus(val);
  }
  virtual const Literal* opPlus(double lhs) const  {
    const Literal* node = new FloatLiteral(static_cast<double>(val) + lhs);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
  virtual const Literal* opPlus(int lhs) const  {
    const Literal* node = new IntLiteral(lhs + val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

  virtual const Literal* operator-(const Literal& rhs) const  {
    return rhs.opSubt(val);
  }
  virtual const Literal* opSubt(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
  virtual const Literal* opSubt(int lhs) const  {
    const Literal* node = new IntLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

  virtual const Literal* operator*(const Literal& rhs) const  {
    return rhs.opMult(val);
  }
  virtual const Literal* opMult(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
  virtual const Literal* opMult(int lhs) const  {
    const Literal* node = new IntLiteral(lhs * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

  virtual const Literal* operator/(const Literal& rhs) const  {
    return rhs.opDiv(val);
  }
  virtual const Literal* opDiv(double lhs) const  {
    if ( val == 0 ) throw std::string("Zero Division Error");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
  virtual const Literal* opDiv(int lhs) const  {
    if ( val == 0 ) throw std::string("Zero Division Error");
    const Literal* node = new IntLiteral(floor(1.0*lhs / val));
    PoolOfNodes::getInstance().add(node);
    return node;
  }

  virtual const Literal* eval() const { return this; }
  virtual void print() const { 
    std::cout << "INT: " << val << std::endl; 
  }

  virtual const Literal* operator|(const Literal& rhs) const {
    return rhs.opBar(val);
  }
  virtual const Literal* opBar(double) const {
	throw std::string("Bar operation : unsupported type for int and float");
  }
  virtual const Literal* opBar(int lhs) const {
    const Literal* node = new IntLiteral(lhs | val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }

  virtual const Literal* operator^(const Literal& rhs) const {
    return rhs.opCircumflex(val);
  }
  virtual const Literal* opCircumflex(double) const {
	throw std::string("Circumflex operation : unsupported type for int and float");
  }
  virtual const Literal* opCircumflex(int lhs) const {
	const Literal* node = new IntLiteral(lhs ^ val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }

  virtual const Literal* operator&(const Literal& rhs) const {
    return rhs.opAmpersand(val);
  }
  virtual const Literal* opAmpersand(double) const {
	throw std::string("Ampersand operation : unsupported type for int and float");
  }
  virtual const Literal* opAmpersand(int lhs) const {
    const Literal* node = new IntLiteral(lhs & val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }

  virtual const Literal* operator<<(const Literal& rhs) const {
    return rhs.opLeftShift(val);
  }
  virtual const Literal* opLeftShift(double) const {
	throw std::string("Leftshift opeartion : unsupported type for int and float");
  }
  virtual const Literal* opLeftShift(int lhs) const {
	const Literal* node = new IntLiteral(lhs << val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }

  virtual const Literal* operator>>(const Literal& rhs) const {
	return rhs.opRightShift(val);
  }
  virtual const Literal* opRightShift(double) const {
	throw std::string("RightShift opeartion : unsupported type for int and  float");
  }
  virtual const Literal* opRightShift(int lhs) const {
	const Literal* node = new IntLiteral(lhs >> val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }

  virtual const Literal* operator%(const Literal& rhs) const {
    return rhs.opMod(val);
  }
  virtual const Literal* opMod(double lhs) const {
	if (!val) {
       throw std::string("Mod operation : zero division error");
	}

    const Literal* node = new FloatLiteral(lhs - val * floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  virtual const Literal* opMod(int lhs) const {
	if (!val) {
		throw std::string("Mod operation : zero division error");
	}

    const Literal* node = new IntLiteral(lhs - val * floor(1.0 * lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  virtual const Literal* floorDiv(const Literal& rhs) const {
	return rhs.opFloorDiv(val);
  }
  virtual const Literal* opFloorDiv(double lhs) const {
	if (!val) {
		throw std::string("Operation // : zero division error");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  virtual const Literal* opFloorDiv(int lhs) const {
	if (!val) {
		throw std::string("Operation // : zero division error");
	}

	const Literal* node = new IntLiteral(static_cast<int>(floor(1.0*lhs / val)));
	PoolOfNodes::getInstance().add(node);
	return node;
  }

  virtual const Literal* powerMul(const Literal& rhs) const {
	return rhs.opPowerMul(val);
  }
  virtual const Literal* opPowerMul(double lhs) const {
	try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw std::string("Operation ** : error");
	}
  }
  virtual const Literal* opPowerMul(int lhs) const {
    try
	{
		const Literal* node = new IntLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw std::string("Operation ** : error");
	}
  }

private:
  int val;
};


