#pragma once
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <iomanip>

#define ENABLE_DEBUG 0

#define DEBUG(fmt) { if (ENABLE_DEBUG) { std::cout<<fmt<<std::endl; } }

const double PRECISION = 0.0000000000000001;

class Literal;

class Node {
public:
  Node() {}
  virtual ~Node() {}
  virtual const Literal* eval() const = 0;
  virtual void print() const { 
    std::cout << "NODE" << std::endl; 
  }
};


