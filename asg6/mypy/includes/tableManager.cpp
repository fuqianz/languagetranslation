#include "tableManager.h"

TableManager& TableManager::getInstance()
{
    static TableManager instance;
    return instance;
}

const Node* TableManager::getEntry(const std::string& name)
{
    for (int index = currentScope ; index >= 0; --index)
    {
        if (tables[index].checkName(name) == true) {
            return tables[index].getValue(name);
        }
    }
    throw std::string("undefined error ") + name + std::string(" is not found");
    return tables[currentScope].getValue(name);
}

void TableManager::insert(const std::string& name, const Node* node)
{
    //printf("table manager: scope %d size %d\n", currentScope, tables.size());
    
    if (tables[currentScope].isGlobal(name) == true)
    {
        //printf("global %s\n",name.c_str());
        tables[0].setValue(name, node);
    }
    else
    {
        tables[currentScope].setValue(name, node);
    }
    //printf("table manager: insert end\n");
}

void TableManager::insert(const std::string& name)
{
    tables[currentScope].addGlobal(name);
}

bool TableManager::checkName(const std::string& name) const
{
    return tables[currentScope].checkName(name);
}

void TableManager::pushScope()
{
    currentScope ++;
    tables.push_back(SymbolTable());
}

void TableManager::popScope()
{
    currentScope --;
    tables.pop_back();
}

