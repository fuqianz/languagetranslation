#ifndef SYMBOLTABLE__H
#define SYMBOLTABLE__H

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <algorithm>

class Node;

class SymbolTable {
public:
  SymbolTable() : table(), globalVar() {}
  void  setValue(const std::string& name, const Node* val);
  const Node* getValue(const std::string& name) const;
  bool  checkName(const std::string& name) const;
  bool isGlobal(const std::string& name) const;
  void addGlobal(const std::string& name);

private:
  std::map<std::string,const Node*> table;
  std::set<std::string> globalVar;
};

#endif
