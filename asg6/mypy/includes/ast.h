#pragma once

//  Declarations for a calculator that builds an AST
//  and a graphical representation of the AST.
//  by Brian Malloy

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "literal.h"

extern void yyerror(const char*);
extern void yyerror(const char*, const char);

class IdentNode : public Node {
public:
  IdentNode(const std::string id) : Node(), ident(id) { } 
  virtual ~IdentNode() {}
  const std::string getIdent() const { return ident; }
  virtual const Literal* eval() const;
private:
  std::string ident;
};

class BinaryNode : public Node {
public:
  BinaryNode(Node* l, Node* r) : Node(), left(l), right(r) {}
  virtual const Literal* eval() const = 0;
  Node* getLeft()  const { return left; }
  Node* getRight() const { return right; }
  BinaryNode(const BinaryNode&) = delete;
  BinaryNode& operator=(const BinaryNode&) = delete;
protected:
  Node *left;
  Node *right;
};
class LessBinaryNode : public BinaryNode {
public:
  LessBinaryNode(Node* left, Node* right) : BinaryNode(left, right) { }
  virtual const Literal* eval() const;
};

class GreaterBinaryNode : public BinaryNode {
public:
  GreaterBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class EqEqualBinaryNode : public BinaryNode {
public:
  EqEqualBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class GreaterEqualBinaryNode : public BinaryNode {
public:
  GreaterEqualBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left | right */
class LessEqualBinaryNode : public BinaryNode {
public:
  LessEqualBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left ^ right*/
class NotEqualBinaryNode : public BinaryNode {
public:
  NotEqualBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class AddEqualNode : public BinaryNode {
public:
  AddEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class SubEqualNode : public BinaryNode {
public:
  SubEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class MulEqualNode : public BinaryNode {
public:
  MulEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class DivEqualNode : public BinaryNode {
public:
  DivEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left | right */
class BarEqualNode : public BinaryNode {
public:
  BarEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left ^ right*/
class CircumflexEqualNode : public BinaryNode {
public:
  CircumflexEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left & right */
class AmpersandEqualNode : public BinaryNode {
public:
  AmpersandEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left << right*/
class LeftShiftEqualNode : public BinaryNode {
public:
  LeftShiftEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left >> right */
class RightShiftEqualNode : public BinaryNode {
public:
  RightShiftEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left % right */
class PercentEqualNode : public BinaryNode {
public:
  PercentEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left // right */
class FloorDivEqualNode : public BinaryNode {
public:
  FloorDivEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left ** right*/
class PowerEqualNode : public BinaryNode {
public:
  PowerEqualNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};


class AsgBinaryNode : public BinaryNode {
public:
  AsgBinaryNode(Node* left, Node* right);
  virtual const Literal* eval() const;
};

class AddBinaryNode : public BinaryNode {
public:
  AddBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class SubBinaryNode : public BinaryNode {
public:
  SubBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class MulBinaryNode : public BinaryNode {
public:
  MulBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

class DivBinaryNode : public BinaryNode {
public:
  DivBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left | right */
class BarBinaryNode : public BinaryNode {
public:
  BarBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left ^ right*/
class CircumflexBinaryNode : public BinaryNode {
public:
  CircumflexBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left & right */
class AmpersandBinaryNode : public BinaryNode {
public:
  AmpersandBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left << right*/
class LeftShiftBinaryNode : public BinaryNode {
public:
  LeftShiftBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left >> right */
class RightShiftBinaryNode : public BinaryNode {
public:
  RightShiftBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left % right */
class PercentBinaryNode : public BinaryNode {
public:
  PercentBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left // right */
class FloorDivBinaryNode : public BinaryNode {
public:
  FloorDivBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* left ** right*/
class PowerBinaryNode : public BinaryNode {
public:
  PowerBinaryNode(Node* left, Node* right) : BinaryNode(left, right) {}
  virtual const Literal* eval() const;
};

/* */
class IfNode : public Node {
public:
    IfNode(Node* _test, Node* _suite) : test(_test), suiteNode(_suite)  { }
    
    virtual const Literal* eval() const;
    IfNode(const IfNode&) = delete;
    IfNode& operator=(const IfNode&) = delete;
private:
    Node* test;
    Node* suiteNode;
};



class PrintNode : public Node {
public:
    PrintNode(std::vector<Node*> *_nodes) : nodes(_nodes)  {}

    virtual const Literal* eval() const;
    virtual void print() const;
    PrintNode(const PrintNode&) = delete;
    PrintNode& operator=(const PrintNode&) = delete;
    ~PrintNode() { delete nodes; }
private:
    std::vector<Node*> *nodes;
};

/* Suite Node  */
class SuiteNode : public Node {

public:
    SuiteNode(Node* node): nodes() {
        nodes->push_back(node);
    }
    SuiteNode(std::vector<Node*> *_nodes) : nodes(_nodes)  {}
    virtual const Literal* eval() const;
    SuiteNode(const SuiteNode&) = delete;
    SuiteNode& operator=(const SuiteNode&) = delete;
    ~SuiteNode() { delete nodes; }
private:
    std::vector<Node*> *nodes;
};

/* ParametersNode */
class ParametersNode : public Node {

public:
    virtual void assignParameters(Node* node) const;
    ParametersNode(std::vector<Node*> *_nodes) : nodes(_nodes)  {}
    virtual const Literal* eval() const;
    ParametersNode(const ParametersNode&) = delete;
    ParametersNode& operator=(const ParametersNode&) = delete;
    ~ParametersNode() { delete nodes; }
private:
    std::vector<Node*> *nodes;
};

/* ParametersNode */
class GlobalNode : public Node {

public:
    virtual void insertGlobalVarToSymbolTable() const;
    GlobalNode(std::vector<Node*> *_nodes) : nodes(_nodes)  {}
    virtual const Literal* eval() const;
    GlobalNode(const GlobalNode&) = delete;
    GlobalNode& operator=(const GlobalNode&) = delete;
    ~GlobalNode() { delete nodes; }
private:
    std::vector<Node*> *nodes;
};

/* function node */
class FuncNode : public Node {

public:
    FuncNode(const std::string id, ParametersNode* parameters, SuiteNode* suite) : paraNode(parameters), suiteNode(suite), fname(id)
    {
    }
    virtual const Literal* eval() const;
    friend class CallFunctionNode;
    FuncNode(const FuncNode&) = delete;
    FuncNode& operator=(const FuncNode&) = delete;
private:
    ParametersNode* paraNode;
    SuiteNode* suiteNode;
    const std::string fname;
};


class ReturnNode : public Node {

public:
    ReturnNode(Node* node, IntLiteral* lit) : val(node), endScope(lit) { }
    virtual const Literal* eval() const;
    ReturnNode(const ReturnNode&) = delete;
    ReturnNode& operator=(const ReturnNode&) = delete;
private:
    Node* val;
    IntLiteral* endScope;
};

class CallFunctionNode : public Node {

public:
    CallFunctionNode(std::string name, ParametersNode* node) : fname(name), pnode(node) {
    }

    const Literal* runStmts(const FuncNode*) const;

    virtual const Literal* eval() const;
    CallFunctionNode(const CallFunctionNode&) = delete;
    CallFunctionNode& operator=(const CallFunctionNode&) = delete;
private:
    std::string fname;
    ParametersNode* pnode;
};

