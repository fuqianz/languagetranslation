#ifndef LITERAL__HH
#define LITERAL__HH

#include "node.h"
#include "poolOfNodes.h"

class Literal : public Node {
public:
  virtual ~Literal() {}

  virtual const Literal* operator+(const Literal& rhs) const =0;
  virtual const Literal* opPlus(double) const =0;
  virtual const Literal* opPlus(int) const =0;
  // virtual const Literal* opPlus(bool) const =0;
  virtual const Literal* opPlus(std::string) const = 0;

  virtual const Literal* operator*(const Literal& rhs) const =0;
  virtual const Literal* opMult(double) const =0;
  virtual const Literal* opMult(int) const =0;
  // virtual const Literal* opMult(bool) const =0;
  virtual const Literal* opMult(std::string) const = 0;

  virtual const Literal* operator-(const Literal& rhs) const =0;
  virtual const Literal* opSubt(double) const =0;
  virtual const Literal* opSubt(int) const =0;
  // virtual const Literal* opSubt(bool) const =0;
  virtual const Literal* opSubt(std::string) const = 0;

  virtual const Literal* operator/(const Literal& rhs) const =0;
  virtual const Literal* opDiv(double) const =0;
  virtual const Literal* opDiv(int) const =0;
  // virtual const Literal* opDiv(bool) const =0;
  virtual const Literal* opDiv(std::string) const = 0;

  virtual const Literal* operator|(const Literal& rhs) const =0;
  virtual const Literal* opBar(double) const =0;
  virtual const Literal* opBar(int) const =0;
  // virtual const Literal* opBar(bool) const =0;
  virtual const Literal* opBar(std::string) const = 0;

  virtual const Literal* operator^(const Literal& rhs) const =0;
  virtual const Literal* opCircumflex(double) const =0;
  virtual const Literal* opCircumflex(int) const =0;
  // virtual const Literal* opCircumflex(bool) const =0;
  virtual const Literal* opCircumflex(std::string) const = 0;

  virtual const Literal* operator&(const Literal& rhs) const =0;
  virtual const Literal* opAmpersand(double) const =0;
  virtual const Literal* opAmpersand(int) const =0;
  // virtual const Literal* opAmpersand(bool) const =0;
  virtual const Literal* opAmpersand(std::string) const = 0;

  virtual const Literal* operator<<(const Literal& rhs) const =0;
  virtual const Literal* opLeftShift(double) const =0;
  virtual const Literal* opLeftShift(int) const =0;
  // virtual const Literal* opLeftShift(bool) const =0;
  virtual const Literal* opLeftShift(std::string) const = 0;

  virtual const Literal* operator>>(const Literal& rhs) const =0;
  virtual const Literal* opRightShift(double) const =0;
  virtual const Literal* opRightShift(int) const =0;
  // virtual const Literal* opRightShift(bool) const =0;
  virtual const Literal* opRightShift(std::string) const = 0;

  virtual const Literal* operator%(const Literal& rhs) const =0;
  virtual const Literal* opMod(double) const = 0;
  virtual const Literal* opMod(int) const = 0;
  // virtual const Literal* opMod(bool) const =0;
  virtual const Literal* opMod(std::string) const = 0;

  virtual const Literal* floorDiv(const Literal& rhs) const = 0;
  virtual const Literal* opFloorDiv(double) const = 0;
  virtual const Literal* opFloorDiv(int) const = 0;
  // virtual const Literal* opFloorDiv(bool) const = 0;
  virtual const Literal* opFloorDiv(std::string) const = 0;

  virtual const Literal* powerMul(const Literal& rhs) const = 0;
  virtual const Literal* opPowerMul(double) const = 0;
  virtual const Literal* opPowerMul(int) const = 0;
  // virtual const Literal* opPowerMul(bool) const = 0;
  virtual const Literal* opPowerMul(std::string) const = 0;

  virtual const Literal* eval() const = 0;
  virtual void print() const { 
    std::cout << "No Way" << std::endl; 
  }

};

class NoneLiteral: public Literal {
public:
  NoneLiteral() {}
  ~NoneLiteral() {}
  const Literal* operator+(const Literal& rhs) const;
  const Literal* opPlus(double) const ;
  const Literal* opPlus(int) const;
  const Literal* opPlus(std::string) const ;

  const Literal* operator*(const Literal& rhs) const;
  const Literal* opMult(double) const;
  const Literal* opMult(int) const;
  const Literal* opMult(std::string) const;

  const Literal* operator-(const Literal& rhs) const;
  const Literal* opSubt(double) const;
  const Literal* opSubt(int) const;
  const Literal* opSubt(std::string) const;

  const Literal* operator/(const Literal& rhs) const;
  const Literal* opDiv(double) const ;
  const Literal* opDiv(int) const ;
  const Literal* opDiv(std::string) const;

  const Literal* operator|(const Literal& rhs) const;
  const Literal* opBar(double) const;
  const Literal* opBar(int) const;
  const Literal* opBar(std::string) const;

  const Literal* operator^(const Literal& rhs) const;
  const Literal* opCircumflex(double) const;
  const Literal* opCircumflex(int) const;
  const Literal* opCircumflex(std::string) const;

  const Literal* operator&(const Literal& rhs) const;
  const Literal* opAmpersand(double) const ;
  const Literal* opAmpersand(int) const;
  const Literal* opAmpersand(std::string) const;

  const Literal* operator<<(const Literal& rhs) const;
  const Literal* opLeftShift(double) const;
  const Literal* opLeftShift(int) const;
  const Literal* opLeftShift(std::string) const;

  const Literal* operator>>(const Literal& rhs) const;
  const Literal* opRightShift(double) const;
  const Literal* opRightShift(int) const;
  const Literal* opRightShift(std::string) const ;

  const Literal* operator%(const Literal& rhs) const;
  const Literal* opMod(double) const ;
  const Literal* opMod(int) const ;
  const Literal* opMod(std::string) const ;

  const Literal* floorDiv(const Literal& rhs) const;
  const Literal* opFloorDiv(double) const ;
  const Literal* opFloorDiv(int) const;
  const Literal* opFloorDiv(std::string) const ;

  const Literal* powerMul(const Literal& rhs) const ;
  const Literal* opPowerMul(double) const ;
  const Literal* opPowerMul(int) const;
  const Literal* opPowerMul(std::string) const;
  
  virtual const Literal* eval() const ;

  virtual void print() const ;

  std::string getVal() const ;

private:
  std::string val = "None";
};

class FloatLiteral: public Literal {
public:
  FloatLiteral(double _val): val(_val) {}
  ~FloatLiteral(){}

   const Literal* operator+(const Literal& rhs) const ;
   const Literal* opPlus(double lhs) const  ;
   const Literal* opPlus(int lhs) const  ;
   const Literal* opPlus(std::string) const ;

   const Literal* operator-(const Literal& rhs) const;
   const Literal* opSubt(double lhs) const ;
   const Literal* opSubt(int lhs) const  ;

   const Literal* opSubt(std::string) const ;

   const Literal* operator*(const Literal& rhs) const ;
   const Literal* opMult(double lhs) const  ;
   const Literal* opMult(int lhs) const ;
   const Literal* opMult(std::string) const {
      throw std::string("Unsupported operation: NoneType * float");
  }

   const Literal* operator/(const Literal& rhs) const  {
    return rhs.opDiv(val);
  }
   const Literal* opDiv(double lhs) const  {
    if ( val == 0 ) throw std::string("Error operation: division by zero");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* opDiv(int lhs) const  {
    if ( val == 0 ) throw std::string("Error operation: division by zero");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* opDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType / float");
  }

   const Literal* eval() const { return this; }
   void print() const { 
    std::cout << "FLOAT: " << std::setprecision(17) << val << std::endl;
  }

   const Literal* operator|(const Literal& rhs) const {
	return rhs.opBar(val);
  }
   const Literal* opBar(double) const {
	throw std::string("Unsupported operation: float | float");
  }
   const Literal* opBar(int) const {
	throw std::string("Unsupported operation: int | float");
  }

   const Literal* opBar(std::string) const {
      throw std::string("Unsupported operation: NoneType + float");
  }

   const Literal* operator^(const Literal& rhs) const {
	return rhs.opCircumflex(val);
  }
   const Literal* opCircumflex(double) const {
	throw std::string("Unsupported operation: float ^ float");
  }
   const Literal* opCircumflex(int) const {
	throw std::string("Unsupported operation: float ^ int");
  }

   const Literal* opCircumflex(std::string) const {
	throw std::string("Unsupported operation: NoneType ^ float");
  }

   const Literal* operator&(const Literal& rhs) const {
	return rhs.opAmpersand(val);
  }
   const Literal* opAmpersand(double) const {
	throw std::string("Unsupported operation: float & float");
  }
   const Literal* opAmpersand(int) const {
	throw std::string("Unsupported opeartion: flaot & int");
  }
   const Literal* opAmpersand(std::string) const {
	throw std::string("Unsupported opeartion: NoneType & float");
  }

   const Literal* operator<<(const Literal& rhs) const {
	return rhs.opLeftShift(val);
  }
   const Literal* opLeftShift(double) const {
	throw std::string("Unsupported operation: float << float");
  }
   const Literal* opLeftShift(int) const {
	throw std::string("Unsupported operation: int << float");
  }
   const Literal* opLeftShift(std::string) const {
	throw std::string("Unsupported operation: NoneType << float");
  }

   const Literal* operator>>(const Literal& rhs) const {
	return rhs.opRightShift(val);
  }
   const Literal* opRightShift(double) const {
	throw std::string("Unsupported operation: float >> float");
  }
   const Literal* opRightShift(int) const {
	throw std::string("Unsupported operation: int >> float");
  }
   const Literal* opRightShift(std::string) const {
	throw std::string("Unsupported operation: NoneType >> float");
  }

   const Literal* operator%(const Literal& rhs) const {
	return rhs.opMod(val);
  }
   const Literal* opMod(double lhs) const {
	if (val > -PRECISION && val < PRECISION) {
	  throw std::string("Error operation: division by zero");
	}
	const Literal* node = new FloatLiteral(lhs - val *  floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opMod(int lhs) const {
	if (val > -PRECISION && val < PRECISION) {
	  throw std::string("Error operation: divison by zero");
	}

	const Literal* node = new FloatLiteral(lhs - val * floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opMod(std::string) const {
      throw std::string("Unsupported operation: NoneType % float");
  }
   const Literal* floorDiv(const Literal& rhs) const {
	return rhs.opFloorDiv(val);
  }
   const Literal* opFloorDiv(double lhs) const {
  if (val > -PRECISION && val < PRECISION) {
		throw std::string("Error operation: division by zero");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opFloorDiv(int lhs) const {
    if (val > -PRECISION && val < PRECISION) {
		throw std::string("Error Operation: division by zero");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opFloorDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType // float");
  }
   const Literal* powerMul(const Literal& rhs) const {
	return rhs.opPowerMul(val);
  }
   const Literal* opPowerMul(double lhs) const {
    try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw msg;
	}
  }
   const Literal* opPowerMul(int lhs) const {
	try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw msg;
    }	
  }

   const Literal* opPowerMul(std::string) const {
     throw std::string("Unsupported operation: NoneType ** float");
  }

  double getVal() const { return val; }

private:
  double val;
};


class IntLiteral: public Literal {
public:
  IntLiteral(int _val): val(_val) {}
  ~IntLiteral() {}
 const Literal* operator+(const Literal& rhs) const  {
    return rhs.opPlus(val);
  }
   const Literal* opPlus(double lhs) const  {
    const Literal* node = new FloatLiteral(static_cast<double>(val) + lhs);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* opPlus(int lhs) const  {
    const Literal* node = new IntLiteral(lhs + val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* opPlus(std::string) const  {
    throw std::string("Unsupported operation: NoneType + int");
  }

   const Literal* operator-(const Literal& rhs) const  {
    return rhs.opSubt(val);
  }
   const Literal* opSubt(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* opSubt(int lhs) const  {
    const Literal* node = new IntLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

   const Literal* opSubt(std::string) const  {
    throw std::string("Unsupported operation: NoneType - int");
  }

   const Literal* operator*(const Literal& rhs) const  {
    return rhs.opMult(val);
  }
   const Literal* opMult(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* opMult(int lhs) const  {
    const Literal* node = new IntLiteral(lhs * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
 
   const Literal* opMult(std::string) const  {
    throw std::string("Unsupported operation: NoneType * int");
  }
   const Literal* operator/(const Literal& rhs) const  {
    return rhs.opDiv(val);
  }
   const Literal* opDiv(double lhs) const  {
    if ( val == 0 ) throw std::string("Error operation: division by zero");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* opDiv(int lhs) const  {
    if ( val == 0 ) throw std::string("Error operation: division by zero");
    const Literal* node = new IntLiteral(floor(1.0*lhs / val));
    PoolOfNodes::getInstance().add(node);
    return node;
  }

   const Literal* opDiv(std::string) const  {
    throw std::string("Unsupported operation: NoneType / int");
  }
   const Literal* eval() const { return this; }
   void print() const { 
    std::cout << "INT: " << val << std::endl; 
  }

   const Literal* operator|(const Literal& rhs) const {
    return rhs.opBar(val);
  }
   const Literal* opBar(double) const {
	throw std::string("Unsupported operation: float | int");
  }
   const Literal* opBar(int lhs) const {
    const Literal* node = new IntLiteral(lhs | val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opBar(std::string) const {
	throw std::string("Unsupported operation: NoneType | int");
  }

   const Literal* operator^(const Literal& rhs) const {
    return rhs.opCircumflex(val);
  }
   const Literal* opCircumflex(double) const {
	throw std::string("Unsupported operation: float ^ int");
  }
   const Literal* opCircumflex(int lhs) const {
	const Literal* node = new IntLiteral(lhs ^ val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opCircumflex(std::string) const {
	throw std::string("Unsupported operation: NoneType ^ int");
  }

   const Literal* operator&(const Literal& rhs) const {
    return rhs.opAmpersand(val);
  }
   const Literal* opAmpersand(double) const {
	throw std::string("Unsupported operation: float & int");
  }
   const Literal* opAmpersand(int lhs) const {
    const Literal* node = new IntLiteral(lhs & val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opAmpersand(std::string) const {
	throw std::string("Unsupported operation: NoneType & int");
  }

   const Literal* operator<<(const Literal& rhs) const {
    return rhs.opLeftShift(val);
  }
   const Literal* opLeftShift(double) const {
	throw std::string("Unsupported opeartion: float << int");
  }
   const Literal* opLeftShift(int lhs) const {
	const Literal* node = new IntLiteral(lhs << val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opLeftShift(std::string) const {
	throw std::string("Unsupported opeartion: NoneType << int");
  }

   const Literal* operator>>(const Literal& rhs) const {
	return rhs.opRightShift(val);
  }
   const Literal* opRightShift(double) const {
	throw std::string("Unsupported operation: float >> int");
  }
   const Literal* opRightShift(int lhs) const {
	const Literal* node = new IntLiteral(lhs >> val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opRightShift(std::string) const {
	throw std::string("Unsupported operation: NoneType >> int");
  }
   const Literal* operator%(const Literal& rhs) const {
    return rhs.opMod(val);
  }
   const Literal* opMod(double lhs) const {
	if (!val) {
       throw std::string("Error operation: division by zero");
	}

    const Literal* node = new FloatLiteral(lhs - val * floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* opMod(int lhs) const {
	if (!val) {
		throw std::string("Error operation: division by zero");
	}

    const Literal* node = new IntLiteral(lhs - val * floor(1.0 * lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }

   const Literal* opMod(std::string) const {
    throw std::string("Unsupported operation: NoneType % int");
  }
 
   const Literal* floorDiv(const Literal& rhs) const {
	return rhs.opFloorDiv(val);
  }
   const Literal* opFloorDiv(double lhs) const {
	if (!val) {
		throw std::string("Error operation: division by zero");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  const Literal* opFloorDiv(int lhs) const {
	if (!val) {
		throw std::string("Error operation: division by zero");
	}

	const Literal* node = new IntLiteral(static_cast<int>(floor(1.0*lhs / val)));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  const Literal* opFloorDiv(std::string) const {
    throw std::string("Unsupported operation: NoneType // int");
  }
  const Literal* powerMul(const Literal& rhs) const {
	return rhs.opPowerMul(val);
  }
  const Literal* opPowerMul(double lhs) const {
	try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw msg;
	}
  }
  const Literal* opPowerMul(int lhs) const {
    try
	{
		const Literal* node = new IntLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw msg;
	}
  }
  const Literal* opPowerMul(std::string) const {
    throw std::string("Unsupported operation: NoneType ** int");
  }

  int getVal() const { return val; }

private:
  int val;
};


class CompLiterals {

public:
  
    static int compare(const Literal* lit1, const Literal* lit2) {
        const FloatLiteral* flt1 = dynamic_cast<const FloatLiteral*>(lit1);
        const IntLiteral* int1 = dynamic_cast<const IntLiteral*>(lit1);
        const NoneLiteral* none1 = dynamic_cast<const NoneLiteral*>(lit1);
        const FloatLiteral* flt2 = dynamic_cast<const FloatLiteral*>(lit2);
        const IntLiteral*  int2 = dynamic_cast<const IntLiteral*>(lit2);
        const NoneLiteral* none2 = dynamic_cast<const NoneLiteral*>(lit2);

        if (flt1 != NULL) 
        {
            if (flt2)
            {
                return compare(flt1->getVal(), flt2->getVal());
            }
            if (int2)
            {
                return compare(flt1->getVal(), int2->getVal());
            }
            if (none2)
            {
                return compare(flt1->getVal(), none2->getVal());
            }
        }
        if (int2 != NULL)
        {
            if (flt2)
            {
                return compare(int1->getVal(), flt2->getVal());
            }
            if (int2)
            {
                return compare(int1->getVal(), int2->getVal());
            }
            if (none2)
            {
                return compare(int1->getVal(), none2->getVal());
            } 
        }
        if (none1 != NULL)
        {
            if (flt2)
            {
                return compare(none1->getVal(), flt2->getVal());
            }
            if (int2)
            {
                return compare(none1->getVal(), int2->getVal());
            }
            if (none2)
            {
                return compare(none1->getVal(), none2->getVal());
            }
        }

        throw std::string("compare literal error");
    }

    static int compare(std::string, std::string) { return 0; }
    static int compare(std::string, int) { return -1; }
    static int compare(std::string, double) { return -1; }
    static int compare(double , std::string) { return 1; }
    static int compare(double f1, int f2) { 
      if (f1 > 1.0*f2) return 1;
      if (f1 < 1.0*f2) return -1;
      return 0;
    }
    static int compare(double f1, double f2) { 
      if (f1 > f2) return 1;
      if (f1 < f2) return -1;
      return 0;
    }
    static int compare(int, std::string) { return 1; }
    static int compare(int f1, double f2) {  
      if (1.0*f1 > f2) return 1;
      if (1.0*f1 < f2) return -1;
      return 0; 
    }
    static int compare(int f1, int f2) { 
      if (f1 > f2) return 1;
      if (f1 < f2) return -1;
      return 0; 
    }
};


class BoolLiteral : public Literal {
public:
  BoolLiteral (bool _val) : val(_val) {}

  virtual ~BoolLiteral() {}
  virtual const Literal* operator+(const Literal& rhs) const {
      return rhs.opPlus(val);
  }

  virtual const Literal* opPlus(double lhs) const {
      const Literal* res = new FloatLiteral((int)val + lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }

  virtual const Literal* opPlus(int lhs) const{
      const Literal* res = new IntLiteral(lhs + val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opPlus(bool lhs) const {
      const Literal* res = new IntLiteral(lhs + val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opPlus(std::string) const {
      throw std::string("Unsupported operation: bool + NoneType");
  }

  virtual const Literal* operator*(const Literal& rhs) const {
      return rhs.opMult(val);
  }
  virtual const Literal* opMult(double lhs) const {
      const Literal* res = new FloatLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMult(int lhs) const {
      const Literal* res = new IntLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMult(bool lhs) const {
      const Literal* res = new IntLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMult(std::string) const{
      throw std::string("Unsupported operation: bool * NoneType"); 
  }

  virtual const Literal* operator-(const Literal& rhs) const {
      return rhs.opSubt(val);
  }
  virtual const Literal* opSubt(double lhs) const {
      const Literal* res = new FloatLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opSubt(int lhs) const {
      const Literal* res = new IntLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opSubt(bool lhs) const {
      const Literal* res = new IntLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opSubt(std::string) const {
      throw std::string("Unsupported operation: NoneType - bool");
  }

  virtual const Literal* operator/(const Literal& rhs) const {
      return rhs.opDiv(val);
  }
  virtual const Literal* opDiv(double lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new FloatLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;    
  }
  virtual const Literal* opDiv(int lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opDiv(bool lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType / bool");
  }

  virtual const Literal* operator|(const Literal& rhs) const {
      return rhs.opBar(val);
  }
  virtual const Literal* opBar(double) const {
      throw std::string("Unsupported opeartion: float | bool");
  }
  virtual const Literal* opBar(int lhs) const {
      const Literal* res = new IntLiteral(lhs | val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opBar(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs | val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opBar(std::string) const {
      throw std::string("Unsupported operation: NoneType | bool");
  }

  virtual const Literal* operator^(const Literal& rhs) const {
      return rhs.opCircumflex(val);
  }
  virtual const Literal* opCircumflex(double) const {
      throw std::string("Unsupported operation: float ^ bool");
  }
  virtual const Literal* opCircumflex(int lhs) const {
      const Literal* res = new IntLiteral(lhs ^ val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opCircumflex(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs ^ val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opCircumflex(std::string) const {
      throw std::string("Unsupported operation: NoneType ^ bool");
  }

  virtual const Literal* operator&(const Literal& rhs) const {
      return rhs.opAmpersand(val);
  }
  virtual const Literal* opAmpersand(double) const {
      throw std::string("Unsupported operation: float & bool");
  } 
  virtual const Literal* opAmpersand(int lhs) const {
      const Literal* res = new IntLiteral(lhs & val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opAmpersand(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs & val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opAmpersand(std::string) const {
      throw std::string("Unsupported operation: NoneType & bool");
  }

  virtual const Literal* operator<<(const Literal& rhs) const {
      return rhs.opLeftShift(val);
  }
  virtual const Literal* opLeftShift(double) const {
      throw std::string("Unsupported operation: float << bool");
  }
  virtual const Literal* opLeftShift(int lhs) const {
      const Literal* res = new IntLiteral(lhs << val );
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opLeftShift(bool lhs) const {
      const Literal* res = new IntLiteral(lhs << val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opLeftShift(std::string) const;
  virtual const Literal* operator>>(const Literal& rhs) const;
  virtual const Literal* opRightShift(double) const;
  virtual const Literal* opRightShift(int lhs) const;
  virtual const Literal* opRightShift(bool lhs) const;
  virtual const Literal* opRightShift(std::string) const;
  virtual const Literal* opMod(double lhs) const;
  virtual const Literal* opMod(int lhs) const;
  virtual const Literal* opMod(bool lhs) const;
  virtual const Literal* opMod(std::string) const;
  virtual const Literal* floorDiv(const Literal& rhs) const;
  virtual const Literal* opFloorDiv(double lhs) const;
  virtual const Literal* opFloorDiv(int lhs) const;
  virtual const Literal* opFloorDiv(bool lhs) const;
  virtual const Literal* opFloorDiv(std::string) const;
  virtual const Literal* powerMul(const Literal& rhs) const;
  virtual const Literal* opPowerMul(double lhs) const;
  virtual const Literal* opPowerMul(int lhs) const;
  virtual const Literal* opPowerMul(bool lhs) const;
  virtual const Literal* opPowerMul(std::string) const;
  virtual const Literal* eval() const;
  virtual void print() const;
 private :
   bool val;
};
#endif
