#ifndef LITERAL__HH
#define LITERAL__HH

#include "node.h"
#include "poolOfNodes.h"
  const Literal* NoneLiteral::operator+(const Literal& rhs) const {
      return rhs.opPlus(val);
  }
  const Literal* NoneLiteral::opPlus(double) const {
      throw std::string("Unsupported opeartion: double + NoneType");
  }
  const Literal* NoneLiteral::opPlus(int) const {
      throw std::string("Unsupported opeartion: int + NoneType");
  }
  const Literal* NoneLiteral::opPlus(std::string) const {
      throw std::string("Unsupported opeartion: NoneType + NoneType");
  }

  const Literal* NoneLiteral::operator*(const Literal& rhs) const {
      return rhs.opMult(val);
  }
  const Literal* NoneLiteral::opMult(double) const {
      throw std::string("Unsupported opeartion: double * NoneType");
  }
  const Literal* NoneLiteral::opMult(int) const {
      throw std::string("Unsupported opeartion: int * NoneType");
  }
  const Literal* NoneLiteral::opMult(std::string) const {
      throw std::string("Unsupported opeartion: NoneType * NoneType");
  }

  const Literal* NoneLiteral::operator-(const Literal& rhs) const {
      return rhs.opSubt(val);
  }
  const Literal* NoneLiteral::opSubt(double) const {
      throw std::string("Unsupported opeartion: double - NoneType");
  }
  const Literal* NoneLiteral::opSubt(int) const {
      throw std::string("Unsupported opeartion: int - NoneType");
  }
  const Literal* NoneLiteral::opSubt(std::string) const {
      throw std::string("Unsupported opeartion: NoneType - NoneType");
  }

  const Literal* NoneLiteral::operator/(const Literal& rhs) const {
      return rhs.opDiv(val);
  }
  const Literal* NoneLiteral::opDiv(double) const {
      throw std::string("Unsupported opeartion: double / NoneType");
  }
  const Literal* NoneLiteral::opDiv(int) const {
      throw std::string("Unsupported opeartion: int / NoneType");
  }
  const Literal* NoneLiteral::opDiv(std::string) const {
      throw std::string("Unsupported opeartion: NoneType / NoneType");
  }

  const Literal* NoneLiteral::operator|(const Literal& rhs) const {
      return rhs.opBar(val);
  }
  const Literal* NoneLiteral::opBar(double) const {
      throw std::string("Unsupported opeartion: double | NoneType");
  }
  const Literal* NoneLiteral::opBar(int) const {
      throw std::string("Unsupported opeartion: int | NoneType");
  }
  const Literal* NoneLiteral::opBar(std::string) const {
      throw std::string("Unsupported opeartion: NoneType | NoneType");
  }

  const Literal* NoneLiteral::operator^(const Literal& rhs) const {
      return rhs.opCircumflex(val);
  }
  const Literal* NoneLiteral::opCircumflex(double) const {
      throw std::string("Unsupported opeartion: double ^ NoneType");
  } 
  const Literal* NoneLiteral::opCircumflex(int) const {
      throw std::string("Unsupported opeartion: int ^ NoneType");
  }
  const Literal* NoneLiteral::opCircumflex(std::string) const {
      throw std::string("Unsupported opeartion: NoneType ^ NoneType");
  }

  const Literal* NoneLiteral::operator&(const Literal& rhs) const {
      return rhs.opAmpersand(val);
  }
  const Literal* NoneLiteral::opAmpersand(double) const {
      throw std::string("Unsupported opeartion: double & NoneType");
  }
  const Literal* NoneLiteral::opAmpersand(int) const {
      throw std::string("Unsupported opeartion: int & NoneType");
  }
  const Literal* NoneLiteral::opAmpersand(std::string) const {
      throw std::string("Unsupported opeartion: NoneType & NoneType");
  }

  const Literal* NoneLiteral::operator<<(const Literal& rhs) const {
      return rhs.opLeftShift(val);
  }
  const Literal* NoneLiteral::opLeftShift(double) const {
      throw std::string("Unsupported opeartion: double << NoneType");
  }
  const Literal* NoneLiteral::opLeftShift(int) const {
      throw std::string("Unsupported opeartion: int << NoneType");
  }
  const Literal* NoneLiteral::opLeftShift(std::string) const {
      throw std::string("Unsupported opeartion: NoneType << NoneType");
  }

  const Literal* NoneLiteral::operator>>(const Literal& rhs) const {
      return rhs.opRightShift(val);
  }
  const Literal* NoneLiteral::opRightShift(double) const {
      throw std::string("Unsupported opeartion: double >> NoneType");
  }
  const Literal* NoneLiteral::opRightShift(int) const {
      throw std::string("Unsupported opeartion: int >> NoneType");
  }
  const Literal* NoneLiteral::opRightShift(std::string) const {
      throw std::string("Unsupported opeartion: NoneType >> NoneType");
  }

  const Literal* NoneLiteral::operator%(const Literal& rhs) const {
      return rhs.opMod(val);
  } 
  const Literal* NoneLiteral::opMod(double) const {
      throw std::string("Unsupported opeartion: double % NoneType");
  }
  const Literal* NoneLiteral::opMod(int) const {
      throw std::string("Unsupported opeartion: int % NoneType");
  }
  const Literal* NoneLiteral::opMod(std::string) const {
      throw std::string("Unsupported opeartion: NoneType % NoneType");
  }

  const Literal* NoneLiteral::floorDiv(const Literal& rhs) const {
      return rhs.opFloorDiv(val);
  }
  const Literal* NoneLiteral::opFloorDiv(double) const {
      throw std::string("Unsupported opeartion: double // NoneType");
  }
  const Literal* NoneLiteral::opFloorDiv(int) const {
      throw std::string("Unsupported opeartion: int // NoneType");
  }
  const Literal* NoneLiteral::opFloorDiv(std::string) const {
      throw std::string("Unsupported opeartion: NoneType // NoneType");
  }

  const Literal* NoneLiteral::powerMul(const Literal& rhs) const {
      return rhs.opPowerMul(val);
  }
  const Literal* NoneLiteral::opPowerMul(double) const {
      throw std::string("Unsupported opeartion: double ** NoneType");
  }
  const Literal* NoneLiteral::opPowerMul(int) const {
      throw std::string("Unsupported opeartion: int ** NoneType");
  }
  const Literal* NoneLiteral::opPowerMul(std::string) const {
      throw std::string("Unsupported opeartion: NoneType ** NoneType");
  }
  
  virtual const Literal* NoneLiteral::eval() const {
      return this;
  }

  virtual void NoneLiteral::print() const { 
    std::cout << "NoneType: "<< val  << std::endl; 
  }

  std::string NoneLiteral::getVal() const { return val; };

const Literal* FloatLiteral::operator+(const Literal& rhs) const  {
    return rhs.opPlus(val);
  }
   const Literal* FloatLiteral::opPlus(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs + val);
    PoolOfNodes::getInstance().add(node);
    return node; 
  }
   const Literal* FloatLiteral::opPlus(int lhs) const  {
    const Literal* node = new FloatLiteral(lhs + val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* FloatLiteral::opPlus(std::string) const {
      throw std::string("Unsupported operation: NoneType + float");
  }

   const Literal* FloatLiteral::operator-(const Literal& rhs) const  {
    return rhs.opSubt(val);
  }
   const Literal* FloatLiteral::opSubt(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* FloatLiteral::opSubt(int lhs) const  {
    const Literal* node = new FloatLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

   const Literal* FloatLiteral::opSubt(std::string) const {
      throw std::string("Unsupported operation: NoneType - float");
  }

   const Literal* FloatLiteral::operator*(const Literal& rhs) const  {
    return rhs.opMult(val);
  }
   const Literal* FloatLiteral::opMult(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* FloatLiteral::opMult(int lhs) const  {
    const Literal* node = new FloatLiteral(static_cast<double>(lhs) * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* FloatLiteral::opMult(std::string) const {
      throw std::string("Unsupported operation: NoneType * float");
  }

   const Literal* FloatLiteral::operator/(const Literal& rhs) const  {
    return rhs.opDiv(val);
  }
   const Literal* FloatLiteral::opDiv(double lhs) const  {
    if ( val == 0 ) throw std::string("Error operation: division by zero");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* FloatLiteral::opDiv(int lhs) const  {
    if ( val == 0 ) throw std::string("Error operation: division by zero");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* FloatLiteral::opDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType / float");
  }

   const Literal* eval() const { return this; }
   void print() const { 
    std::cout << "FLOAT: " << std::setprecision(17) << val << std::endl;
  }

   const Literal* FloatLiteral::operator|(const Literal& rhs) const {
	return rhs.opBar(val);
  }
   const Literal* FloatLiteral::opBar(double) const {
	throw std::string("Unsupported operation: float | float");
  }
   const Literal* FloatLiteral::opBar(int) const {
	throw std::string("Unsupported operation: int | float");
  }

   const Literal* FloatLiteral::opBar(std::string) const {
      throw std::string("Unsupported operation: NoneType + float");
  }

   const Literal* FloatLiteral::operator^(const Literal& rhs) const {
	return rhs.opCircumflex(val);
  }
   const Literal* FloatLiteral::opCircumflex(double) const {
	throw std::string("Unsupported operation: float ^ float");
  }
   const Literal* FloatLiteral::opCircumflex(int) const {
	throw std::string("Unsupported operation: float ^ int");
  }

   const Literal* FloatLiteral::opCircumflex(std::string) const {
	throw std::string("Unsupported operation: NoneType ^ float");
  }

   const Literal* FloatLiteral::operator&(const Literal& rhs) const {
	return rhs.opAmpersand(val);
  }
   const Literal* FloatLiteral::opAmpersand(double) const {
	throw std::string("Unsupported operation: float & float");
  }
   const Literal* FloatLiteral::opAmpersand(int) const {
	throw std::string("Unsupported opeartion: flaot & int");
  }
   const Literal* FloatLiteral::opAmpersand(std::string) const {
	throw std::string("Unsupported opeartion: NoneType & float");
  }

   const Literal* FloatLiteral::operator<<(const Literal& rhs) const {
	return rhs.opLeftShift(val);
  }
   const Literal* FloatLiteral::opLeftShift(double) const {
	throw std::string("Unsupported operation: float << float");
  }
   const Literal* FloatLiteral::opLeftShift(int) const {
	throw std::string("Unsupported operation: int << float");
  }
   const Literal* FloatLiteral::opLeftShift(std::string) const {
	throw std::string("Unsupported operation: NoneType << float");
  }

   const Literal* FloatLiteral::operator>>(const Literal& rhs) const {
	return rhs.opRightShift(val);
  }
   const Literal* FloatLiteral::opRightShift(double) const {
	throw std::string("Unsupported operation: float >> float");
  }
   const Literal* FloatLiteral::opRightShift(int) const {
	throw std::string("Unsupported operation: int >> float");
  }
   const Literal* FloatLiteral::opRightShift(std::string) const {
	throw std::string("Unsupported operation: NoneType >> float");
  }

   const Literal* FloatLiteral::operator%(const Literal& rhs) const {
	return rhs.opMod(val);
  }
   const Literal* FloatLiteral::opMod(double lhs) const {
	if (val > -PRECISION && val < PRECISION) {
	  throw std::string("Error operation: division by zero");
	}
	const Literal* node = new FloatLiteral(lhs - val *  floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* FloatLiteral::opMod(int lhs) const {
	if (val > -PRECISION && val < PRECISION) {
	  throw std::string("Error operation: divison by zero");
	}

	const Literal* node = new FloatLiteral(lhs - val * floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* FloatLiteral::opMod(std::string) const {
      throw std::string("Unsupported operation: NoneType % float");
  }
   const Literal* floorDiv(const Literal& rhs) const {
	return rhs.opFloorDiv(val);
  }
   const Literal* FloatLiteral::opFloorDiv(double lhs) const {
  if (val > -PRECISION && val < PRECISION) {
		throw std::string("Error operation: division by zero");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* FloatLiteral::opFloorDiv(int lhs) const {
    if (val > -PRECISION && val < PRECISION) {
		throw std::string("Error Operation: division by zero");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* FloatLiteral::opFloorDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType // float");
  }
   const Literal* powerMul(const Literal& rhs) const {
	return rhs.opPowerMul(val);
  }
   const Literal* FloatLiteral::opPowerMul(double lhs) const {
    try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw msg;
	}
  }
   const Literal* FloatLiteral::opPowerMul(int lhs) const {
	try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw msg;
    }	
  }

   const Literal* FloatLiteral::opPowerMul(std::string) const {
     throw std::string("Unsupported operation: NoneType ** float");
  }

  double FloatLiteral::getVal() const { return val; }


 const Literal* IntLiteral::operator+(const Literal& rhs) const  {
    return rhs.opPlus(val);
  }
   const Literal* IntLiteral::opPlus(double lhs) const  {
    const Literal* node = new FloatLiteral(static_cast<double>(val) + lhs);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* IntLiteral::opPlus(int lhs) const  {
    const Literal* node = new IntLiteral(lhs + val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* IntLiteral::opPlus(std::string) const  {
    throw std::string("Unsupported operation: NoneType + int");
  }

   const Literal* IntLiteral::operator-(const Literal& rhs) const  {
    return rhs.opSubt(val);
  }
   const Literal* IntLiteral::opSubt(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* IntLiteral::opSubt(int lhs) const  {
    const Literal* node = new IntLiteral(lhs - val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }

   const Literal* IntLiteral::opSubt(std::string) const  {
    throw std::string("Unsupported operation: NoneType - int");
  }

   const Literal* IntLiteral::operator*(const Literal& rhs) const  {
    return rhs.opMult(val);
  }
   const Literal* IntLiteral::opMult(double lhs) const  {
    const Literal* node = new FloatLiteral(lhs * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* IntLiteral::opMult(int lhs) const  {
    const Literal* node = new IntLiteral(lhs * val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
 
   const Literal* IntLiteral::opMult(std::string) const  {
    throw std::string("Unsupported operation: NoneType * int");
  }
   const Literal* IntLiteral::operator/(const Literal& rhs) const  {
    return rhs.opDiv(val);
  }
   const Literal* IntLiteral::opDiv(double lhs) const  {
    if ( val == 0 ) throw std::string("Error operation: division by zero");
    const Literal* node = new FloatLiteral(lhs / val);
    PoolOfNodes::getInstance().add(node);
    return node;
  }
   const Literal* IntLiteral::opDiv(int lhs) const  {
    if ( val == 0 ) throw std::string("Error operation: division by zero");
    const Literal* node = new IntLiteral(floor(1.0*lhs / val));
    PoolOfNodes::getInstance().add(node);
    return node;
  }

   const Literal* IntLiteral::opDiv(std::string) const  {
    throw std::string("Unsupported operation: NoneType / int");
  }
   const Literal* eval() const { return this; }
   void print() const { 
    std::cout << "INT: " << val << std::endl; 
  }

   const Literal* IntLiteral::operator|(const Literal& rhs) const {
    return rhs.opBar(val);
  }
   const Literal* IntLiteral::opBar(double) const {
	throw std::string("Unsupported operation: float | int");
  }
   const Literal* IntLiteral::opBar(int lhs) const {
    const Literal* node = new IntLiteral(lhs | val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* IntLiteral::opBar(std::string) const {
	throw std::string("Unsupported operation: NoneType | int");
  }

   const Literal* IntLiteral::operator^(const Literal& rhs) const {
    return rhs.opCircumflex(val);
  }
   const Literal* IntLiteral::opCircumflex(double) const {
	throw std::string("Unsupported operation: float ^ int");
  }
   const Literal* IntLiteral::opCircumflex(int lhs) const {
	const Literal* node = new IntLiteral(lhs ^ val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* IntLiteral::opCircumflex(std::string) const {
	throw std::string("Unsupported operation: NoneType ^ int");
  }

   const Literal* IntLiteral::operator&(const Literal& rhs) const {
    return rhs.opAmpersand(val);
  }
   const Literal* IntLiteral::opAmpersand(double) const {
	throw std::string("Unsupported operation: float & int");
  }
   const Literal* IntLiteral::opAmpersand(int lhs) const {
    const Literal* node = new IntLiteral(lhs & val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* IntLiteral::opAmpersand(std::string) const {
	throw std::string("Unsupported operation: NoneType & int");
  }

   const Literal* IntLiteral::operator<<(const Literal& rhs) const {
    return rhs.opLeftShift(val);
  }
   const Literal* IntLiteral::opLeftShift(double) const {
	throw std::string("Unsupported opeartion: float << int");
  }
   const Literal* IntLiteral::opLeftShift(int lhs) const {
	const Literal* node = new IntLiteral(lhs << val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* IntLiteral::opLeftShift(std::string) const {
	throw std::string("Unsupported opeartion: NoneType << int");
  }

   const Literal* IntLiteral::operator>>(const Literal& rhs) const {
	return rhs.opRightShift(val);
  }
   const Literal* IntLiteral::opRightShift(double) const {
	throw std::string("Unsupported operation: float >> int");
  }
   const Literal* IntLiteral::opRightShift(int lhs) const {
	const Literal* node = new IntLiteral(lhs >> val);
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* IntLiteral::opRightShift(std::string) const {
	throw std::string("Unsupported operation: NoneType >> int");
  }
   const Literal* IntLiteral::operator%(const Literal& rhs) const {
    return rhs.opMod(val);
  }
   const Literal* IntLiteral::opMod(double lhs) const {
	if (!val) {
       throw std::string("Error operation: division by zero");
	}

    const Literal* node = new FloatLiteral(lhs - val * floor(lhs/val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
   const Literal* IntLiteral::opMod(int lhs) const {
	if (!val) {
		throw std::string("Error operation: division by zero");
	}

    const Literal* node = new IntLiteral(lhs - val * floor(1.0 * lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }

   const Literal* IntLiteral::opMod(std::string) const {
    throw std::string("Unsupported operation: NoneType % int");
  }
 
   const Literal* floorDiv(const Literal& rhs) const {
	return rhs.opFloorDiv(val);
  }
   const Literal* IntLiteral::opFloorDiv(double lhs) const {
	if (!val) {
		throw std::string("Error operation: division by zero");
	}
	
	const Literal* node = new FloatLiteral(floor(lhs / val));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  const Literal* IntLiteral::opFloorDiv(int lhs) const {
	if (!val) {
		throw std::string("Error operation: division by zero");
	}

	const Literal* node = new IntLiteral(static_cast<int>(floor(1.0*lhs / val)));
	PoolOfNodes::getInstance().add(node);
	return node;
  }
  const Literal* IntLiteral::opFloorDiv(std::string) const {
    throw std::string("Unsupported operation: NoneType // int");
  }
  const Literal* powerMul(const Literal& rhs) const {
	return rhs.opPowerMul(val);
  }
  const Literal* IntLiteral::opPowerMul(double lhs) const {
	try
	{
		const Literal* node = new FloatLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw msg;
	}
  }
  const Literal* IntLiteral::opPowerMul(int lhs) const {
    try
	{
		const Literal* node = new IntLiteral(pow(lhs, val));
		PoolOfNodes::getInstance().add(node);
		return node;
	}
	catch (const std::string& msg) 
	{
		throw msg;
	}
  }
  const Literal* IntLiteral::opPowerMul(std::string) const {
    throw std::string("Unsupported operation: NoneType ** int");
  }

  int IntLiteral::getVal() const { return val; }

  virtual const Literal* BoolLiteral::operator+(const Literal& rhs) const {
      return rhs.opPlus(val);
  }

  virtual const Literal* BoolLiteral::opPlus(double lhs) const {
      const Literal* res = new FloatLiteral((int)val + lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }

  virtual const Literal* BoolLiteral::opPlus(int lhs) const{
      const Literal* res = new IntLiteral(lhs + val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opPlus(bool lhs) const {
      const Literal* res = new IntLiteral(lhs + val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opPlus(std::string) const {
      throw std::string("Unsupported operation: bool + NoneType");
  }

  virtual const Literal* BoolLiteral::operator*(const Literal& rhs) const {
      return rhs.opMult(val);
  }
  virtual const Literal* BoolLiteral::opMult(double lhs) const {
      const Literal* res = new FloatLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opMult(int lhs) const {
      const Literal* res = new IntLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opMult(bool lhs) const {
      const Literal* res = new IntLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opMult(std::string) const{
      throw std::string("Unsupported operation: bool * NoneType"); 
  }

  virtual const Literal* BoolLiteral::operator-(const Literal& rhs) const {
      return rhs.opSubt(val);
  }
  virtual const Literal* BoolLiteral::opSubt(double lhs) const {
      const Literal* res = new FloatLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opSubt(int lhs) const {
      const Literal* res = new IntLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opSubt(bool lhs) const {
      const Literal* res = new IntLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opSubt(std::string) const {
      throw std::string("Unsupported operation: NoneType - bool");
  }

  virtual const Literal* BoolLiteral::operator/(const Literal& rhs) const {
      return rhs.opDiv(val);
  }
  virtual const Literal* BoolLiteral::opDiv(double lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new FloatLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;    
  }
  virtual const Literal* BoolLiteral::opDiv(int lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opDiv(bool lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType / bool");
  }

  virtual const Literal* BoolLiteral::operator|(const Literal& rhs) const {
      return rhs.opBar(val);
  }
  virtual const Literal* BoolLiteral::opBar(double) const {
      throw std::string("Unsupported opeartion: float | bool");
  }
  virtual const Literal* BoolLiteral::opBar(int lhs) const {
      const Literal* res = new IntLiteral(lhs | val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opBar(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs | val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opBar(std::string) const {
      throw std::string("Unsupported operation: NoneType | bool");
  }

  virtual const Literal* BoolLiteral::operator^(const Literal& rhs) const {
      return rhs.opCircumflex(val);
  }
  virtual const Literal* BoolLiteral::opCircumflex(double) const {
      throw std::string("Unsupported operation: float ^ bool");
  }
  virtual const Literal* BoolLiteral::opCircumflex(int lhs) const {
      const Literal* res = new IntLiteral(lhs ^ val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opCircumflex(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs ^ val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opCircumflex(std::string) const {
      throw std::string("Unsupported operation: NoneType ^ bool");
  }

  virtual const Literal* BoolLiteral::operator&(const Literal& rhs) const {
      return rhs.opAmpersand(val);
  }
  virtual const Literal* BoolLiteral::opAmpersand(double) const {
      throw std::string("Unsupported operation: float & bool");
  } 
  virtual const Literal* BoolLiteral::opAmpersand(int lhs) const {
      const Literal* res = new IntLiteral(lhs & val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opAmpersand(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs & val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opAmpersand(std::string) const {
      throw std::string("Unsupported operation: NoneType & bool");
  }

  virtual const Literal* BoolLiteral::operator<<(const Literal& rhs) const {
      return rhs.opLeftShift(val);
  }
  virtual const Literal* BoolLiteral::opLeftShift(double) const {
      throw std::string("Unsupported operation: float << bool");
  }
  virtual const Literal* BoolLiteral::opLeftShift(int lhs) const {
      const Literal* res = new IntLiteral(lhs << val );
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opLeftShift(bool lhs) const {
      const Literal* res = new IntLiteral(lhs << val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opLeftShift(std::string) const {
      throw std::string("Unsupported opeartion: NoneType << bool");
  }

  virtual const Literal* BoolLiteral::operator>>(const Literal& rhs) const {
      return rhs.opRightShift(val);
  }
  virtual const Literal* BoolLiteral::opRightShift(double) const {
      throw std::string("Unsupported operation: float >> bool");
  }
  virtual const Literal* BoolLiteral::opRightShift(int lhs) const {
      const Literal* res = new IntLiteral(lhs >> val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opRightShift(bool lhs) const {
      const Literal* res = new IntLiteral(lhs >> val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opRightShift(std::string) const {
      throw std::string("Unsupported operation: NoneType >> bool");
  }

  virtual const Literal* BoolLiteral::operator%(const Literal& rhs) const {
     
      return rhs.opMod(val);
  }
  virtual const Literal* BoolLiteral::opMod(double lhs) const {
  
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
    
      const Literal* res = new FloatLiteral(lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opMod(int lhs) const {
   
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
      const Literal* res = new IntLiteral(lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opMod(bool lhs) const {
    
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
   const Literal* res = new IntLiteral(lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opMod(std::string) const {
      throw std::string("Unsupported operation: NoneType % bool");
  }

  virtual const Literal* floorDiv(const Literal& rhs) const {
      return rhs.opFloorDiv(val);
  }
  virtual const Literal* BoolLiteral::opFloorDiv(double lhs) const {
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
      const Literal* res = new FloatLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opFloorDiv(int lhs) const {
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opFloorDiv(bool lhs) const {  
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opFloorDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType // bool");
  }

  virtual const Literal* powerMul(const Literal& rhs) const {
      return rhs.opPowerMul(val);
  }
  virtual const Literal* BoolLiteral::opPowerMul(double lhs) const {
      int tval = 1;
      if (!val) {
          tval = 0;
      }
      const Literal* res = new FloatLiteral(pow(lhs, tval));
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opPowerMul(int lhs) const {
      int tval = 1;
      if (!val) {
          tval = 0;
      }
      const Literal* res = new IntLiteral(pow(lhs, tval));
      PoolOfNodes::getInstance().add(res);
      return res; 
  }
  virtual const Literal* BoolLiteral::opPowerMul(bool lhs) const {
      const Literal* res = nullptr;
      if (lhs == 0) {
          res = new IntLiteral(0);
      }
      else {
          res = new IntLiteral(1);
      }

      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* BoolLiteral::opPowerMul(std::string) const {
      throw std::string("Unsupported operation: NoneType ** bool");
  }

  virtual const Literal* eval() const
  {
      return this;
  }
  virtual void print() const
  { 
    if ( val )
    {
        std::cout << "True" << std::endl;
    }
    else
    {
        std::cout << "False" << std::endl;
    }
  }

/*
class BoolLiteral : public Literal {
public:
  BoolLiteral (bool _val) : val(_val) {}

  virtual ~BoolLiteral() {}
  virtual const Literal* operator+(const Literal& rhs) const {
      return rhs.opPlus(val);
  }

  virtual const Literal* opPlus(double lhs) const {
      const Literal* res = new FloatLiteral((int)val + lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }

  virtual const Literal* opPlus(int lhs) const{
      const Literal* res = new IntLiteral(lhs + val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opPlus(bool lhs) const {
      const Literal* res = new IntLiteral(lhs + val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opPlus(std::string) const {
      throw std::string("Unsupported operation: bool + NoneType");
  }

  virtual const Literal* operator*(const Literal& rhs) const {
      return rhs.opMult(val);
  }
  virtual const Literal* opMult(double lhs) const {
      const Literal* res = new FloatLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMult(int lhs) const {
      const Literal* res = new IntLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMult(bool lhs) const {
      const Literal* res = new IntLiteral(val * lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMult(std::string) const{
      throw std::string("Unsupported operation: bool * NoneType"); 
  }

  virtual const Literal* operator-(const Literal& rhs) const {
      return rhs.opSubt(val);
  }
  virtual const Literal* opSubt(double lhs) const {
      const Literal* res = new FloatLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opSubt(int lhs) const {
      const Literal* res = new IntLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opSubt(bool lhs) const {
      const Literal* res = new IntLiteral(lhs - val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opSubt(std::string) const {
      throw std::string("Unsupported operation: NoneType - bool");
  }

  virtual const Literal* operator/(const Literal& rhs) const {
      return rhs.opDiv(val);
  }
  virtual const Literal* opDiv(double lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new FloatLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;    
  }
  virtual const Literal* opDiv(int lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opDiv(bool lhs) const {
      if (!val)
      {
          throw std::string("Error opeartion: divison by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType / bool");
  }

  virtual const Literal* operator|(const Literal& rhs) const {
      return rhs.opBar(val);
  }
  virtual const Literal* opBar(double) const {
      throw std::string("Unsupported opeartion: float | bool");
  }
  virtual const Literal* opBar(int lhs) const {
      const Literal* res = new IntLiteral(lhs | val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opBar(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs | val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opBar(std::string) const {
      throw std::string("Unsupported operation: NoneType | bool");
  }

  virtual const Literal* operator^(const Literal& rhs) const {
      return rhs.opCircumflex(val);
  }
  virtual const Literal* opCircumflex(double) const {
      throw std::string("Unsupported operation: float ^ bool");
  }
  virtual const Literal* opCircumflex(int lhs) const {
      const Literal* res = new IntLiteral(lhs ^ val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opCircumflex(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs ^ val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opCircumflex(std::string) const {
      throw std::string("Unsupported operation: NoneType ^ bool");
  }

  virtual const Literal* operator&(const Literal& rhs) const {
      return rhs.opAmpersand(val);
  }
  virtual const Literal* opAmpersand(double) const {
      throw std::string("Unsupported operation: float & bool");
  } 
  virtual const Literal* opAmpersand(int lhs) const {
      const Literal* res = new IntLiteral(lhs & val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opAmpersand(bool lhs) const {
      const Literal* res = new BoolLiteral(lhs & val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opAmpersand(std::string) const {
      throw std::string("Unsupported operation: NoneType & bool");
  }

  virtual const Literal* operator<<(const Literal& rhs) const {
      return rhs.opLeftShift(val);
  }
  virtual const Literal* opLeftShift(double) const {
      throw std::string("Unsupported operation: float << bool");
  }
  virtual const Literal* opLeftShift(int lhs) const {
      const Literal* res = new IntLiteral(lhs << val );
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opLeftShift(bool lhs) const {
      const Literal* res = new IntLiteral(lhs << val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opLeftShift(std::string) const {
      throw std::string("Unsupported opeartion: NoneType << bool");
  }

  virtual const Literal* operator>>(const Literal& rhs) const {
      return rhs.opRightShift(val);
  }
  virtual const Literal* opRightShift(double) const {
      throw std::string("Unsupported operation: float >> bool");
  }
  virtual const Literal* opRightShift(int lhs) const {
      const Literal* res = new IntLiteral(lhs >> val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opRightShift(bool lhs) const {
      const Literal* res = new IntLiteral(lhs >> val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opRightShift(std::string) const {
      throw std::string("Unsupported operation: NoneType >> bool");
  }

  virtual const Literal* operator%(const Literal& rhs) const {
     
      return rhs.opMod(val);
  }
  virtual const Literal* opMod(double lhs) const {
  
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
    
      const Literal* res = new FloatLiteral(lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMod(int lhs) const {
   
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
      const Literal* res = new IntLiteral(lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMod(bool lhs) const {
    
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
   const Literal* res = new IntLiteral(lhs);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opMod(std::string) const {
      throw std::string("Unsupported operation: NoneType % bool");
  }

  virtual const Literal* floorDiv(const Literal& rhs) const {
      return rhs.opFloorDiv(val);
  }
  virtual const Literal* opFloorDiv(double lhs) const {
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
      const Literal* res = new FloatLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opFloorDiv(int lhs) const {
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opFloorDiv(bool lhs) const {  
      if (!val){
          throw std::string("Error opeartion: division by zero");
      }
      const Literal* res = new IntLiteral(lhs / val);
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opFloorDiv(std::string) const {
      throw std::string("Unsupported operation: NoneType // bool");
  }

  virtual const Literal* powerMul(const Literal& rhs) const {
      return rhs.opPowerMul(val);
  }
  virtual const Literal* opPowerMul(double lhs) const {
      int tval = 1;
      if (!val) {
          tval = 0;
      }
      const Literal* res = new FloatLiteral(pow(lhs, tval));
      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opPowerMul(int lhs) const {
      int tval = 1;
      if (!val) {
          tval = 0;
      }
      const Literal* res = new IntLiteral(pow(lhs, tval));
      PoolOfNodes::getInstance().add(res);
      return res; 
  }
  virtual const Literal* opPowerMul(bool lhs) const {
      const Literal* res = nullptr;
      if (lhs == 0) {
          res = new IntLiteral(0);
      }
      else {
          res = new IntLiteral(1);
      }

      PoolOfNodes::getInstance().add(res);
      return res;
  }
  virtual const Literal* opPowerMul(std::string) const {
      throw std::string("Unsupported operation: NoneType ** bool");
  }

  virtual const Literal* eval() const
  {
      return this;
  }
  virtual void print() const
  { 
    if ( val )
    {
        std::cout << "True" << std::endl;
    }
    else
    {
        std::cout << "False" << std::endl;
    }
  }
 private :
   bool val;
};
*/
#endif
