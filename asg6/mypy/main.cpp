#include <iostream>
#include <string>
#include "includes/symbolTable.h"
#include "includes/poolOfNodes.h"

extern int yyparse();
extern void init_scanner(FILE *);
extern void free_token();

static FILE * open_file(const char *filename)
{
    FILE *file = fopen(filename, "r");
    if(!file)
    {
        fprintf(stderr, "Could not open file \"%s\"\n", filename);
        exit(EXIT_FAILURE);
    }

    return file;
}

int main(int argc, char * argv[])
{
    FILE *input_file = stdin;
    if (argc > 1)
    {
        input_file = open_file(argv[1]);
    }

    init_scanner(input_file);
    try
    {
        if ( yyparse() == 0 ) 
        {
            std::cout << "Program syntactically correct" << std::endl;
        }
    }
    catch ( const std::string& msg ) 
    {
        std::cout << "Oops: " << msg << std::endl;
    }
    catch (...)
    {
        std::cout << "Uncaught Exception" << std::endl;
    }
    PoolOfNodes::getInstance().drainThePool();
    fclose(input_file);
    free_token();
    return 0;
}
