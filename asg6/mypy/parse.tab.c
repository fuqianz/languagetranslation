/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 2 "includes/parse.y" /* yacc.c:339  */

#include <iostream>
#include <map>
#include <vector>
#include <string.h>
#include "includes/tableManager.h"
#include "includes/ast.h"

int yylex (void);
extern char *yytext;

void yyerror (const char *);
PoolOfNodes& pool = PoolOfNodes::getInstance();
TableManager& tableManager = TableManager::getInstance();

#line 82 "parse.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parse.tab.h".  */
#ifndef YY_YY_PARSE_TAB_H_INCLUDED
# define YY_YY_PARSE_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    AMPEREQUAL = 258,
    AMPERSAND = 259,
    AND = 260,
    AS = 261,
    ASSERT = 262,
    AT = 263,
    BACKQUOTE = 264,
    BAR = 265,
    BREAK = 266,
    CIRCUMFLEX = 267,
    CIRCUMFLEXEQUAL = 268,
    CLASS = 269,
    COLON = 270,
    COMMA = 271,
    CONTINUE = 272,
    DEDENT = 273,
    DEF = 274,
    DEL = 275,
    DOT = 276,
    DOUBLESLASH = 277,
    DOUBLESLASHEQUAL = 278,
    DOUBLESTAR = 279,
    DOUBLESTAREQUAL = 280,
    ELIF = 281,
    ELSE = 282,
    ENDMARKER = 283,
    EQUAL = 284,
    EXCEPT = 285,
    EXEC = 286,
    FINALLY = 287,
    FOR = 288,
    FROM = 289,
    GLOBAL = 290,
    IF = 291,
    IMPORT = 292,
    INDENT = 293,
    LAMBDA = 294,
    LBRACE = 295,
    LEFTSHIFT = 296,
    LEFTSHIFTEQUAL = 297,
    LPAR = 298,
    LSQB = 299,
    MINEQUAL = 300,
    MINUS = 301,
    NEWLINE = 302,
    NOT = 303,
    OR = 304,
    PASS = 305,
    PERCENT = 306,
    PERCENTEQUAL = 307,
    PLUS = 308,
    PLUSEQUAL = 309,
    PRINT = 310,
    RAISE = 311,
    RBRACE = 312,
    RETURN = 313,
    RIGHTSHIFT = 314,
    RIGHTSHIFTEQUAL = 315,
    RPAR = 316,
    RSQB = 317,
    SEMI = 318,
    SLASH = 319,
    SLASHEQUAL = 320,
    STAR = 321,
    STAREQUAL = 322,
    STRING = 323,
    TILDE = 324,
    TRY = 325,
    VBAREQUAL = 326,
    WHILE = 327,
    WITH = 328,
    YIELD = 329,
    NAME = 330,
    FLOAT = 331,
    INT = 332,
    LESS = 333,
    GREATER = 334,
    GRLT = 335,
    GREATEREQUAL = 336,
    LESSEQUAL = 337,
    NOTEQUAL = 338,
    EQEQUAL = 339,
    IN = 340,
    NOT_IN = 341,
    IS = 342,
    IS_NOT = 343
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 18 "includes/parse.y" /* yacc.c:355  */

    Node* node;
    int intNumber;
    double fltNumber;
    char* id;
    std::vector<Node*> *vec;

#line 219 "parse.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_PARSE_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 250 "parse.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   870

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  89
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  140
/* YYNRULES -- Number of rules.  */
#define YYNRULES  314
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  478

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   343

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    59,    59,    62,    65,    66,    79,    80,    83,    84,
      87,    91,    97,    98,   101,   102,   105,   118,   122,   128,
     132,   155,   159,   165,   186,   192,   193,   196,   197,   200,
     201,   204,   211,   217,   218,   221,   222,   225,   229,   235,
     239,   245,   249,   255,   259,   263,   267,   271,   275,   279,
     283,   287,   293,   330,   350,   351,   357,   378,   381,   386,
     391,   396,   401,   406,   411,   416,   421,   426,   431,   436,
     443,   448,   454,   463,   467,   480,   484,   485,   488,   489,
     492,   495,   498,   499,   500,   501,   502,   505,   508,   511,
     518,   529,   532,   533,   536,   537,   540,   541,   544,   545,
     548,   551,   554,   555,   558,   559,   560,   563,   564,   567,
     568,   571,   572,   575,   576,   579,   580,   583,   584,   587,
     602,   618,   621,   622,   625,   626,   629,   630,   631,   632,
     633,   634,   635,   636,   639,   643,   652,   656,   660,   661,
     664,   665,   668,   669,   672,   673,   676,   677,   680,   681,
     684,   687,   688,   691,   692,   695,   696,   699,   700,   703,
     704,   707,   712,   718,   723,   730,   731,   734,   735,   738,
     739,   742,   743,   746,   750,   753,   754,   757,   761,   764,
     768,   771,   772,   778,   782,   799,   800,   801,   802,   803,
     804,   805,   806,   807,   808,   809,   812,   816,   828,   832,
     841,   845,   856,   860,   882,   887,   894,   899,   916,   921,
     928,   932,   958,   963,   968,   973,   980,  1018,  1024,  1028,
    1032,  1038,  1044,  1062,  1066,  1070,  1074,  1078,  1082,  1086,
    1105,  1111,  1117,  1123,  1127,  1133,  1137,  1143,  1144,  1147,
    1148,  1151,  1152,  1155,  1156,  1159,  1163,  1169,  1170,  1173,
    1181,  1185,  1193,  1194,  1197,  1198,  1201,  1202,  1203,  1206,
    1207,  1210,  1211,  1214,  1215,  1218,  1219,  1222,  1223,  1226,
    1230,  1236,  1237,  1240,  1241,  1244,  1245,  1248,  1249,  1252,
    1253,  1256,  1257,  1260,  1272,  1282,  1286,  1287,  1290,  1291,
    1294,  1298,  1302,  1308,  1312,  1321,  1322,  1325,  1326,  1329,
    1330,  1333,  1334,  1337,  1338,  1341,  1342,  1345,  1346,  1349,
    1350,  1353,  1354,  1357,  1358
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "AMPEREQUAL", "AMPERSAND", "AND", "AS",
  "ASSERT", "AT", "BACKQUOTE", "BAR", "BREAK", "CIRCUMFLEX",
  "CIRCUMFLEXEQUAL", "CLASS", "COLON", "COMMA", "CONTINUE", "DEDENT",
  "DEF", "DEL", "DOT", "DOUBLESLASH", "DOUBLESLASHEQUAL", "DOUBLESTAR",
  "DOUBLESTAREQUAL", "ELIF", "ELSE", "ENDMARKER", "EQUAL", "EXCEPT",
  "EXEC", "FINALLY", "FOR", "FROM", "GLOBAL", "IF", "IMPORT", "INDENT",
  "LAMBDA", "LBRACE", "LEFTSHIFT", "LEFTSHIFTEQUAL", "LPAR", "LSQB",
  "MINEQUAL", "MINUS", "NEWLINE", "NOT", "OR", "PASS", "PERCENT",
  "PERCENTEQUAL", "PLUS", "PLUSEQUAL", "PRINT", "RAISE", "RBRACE",
  "RETURN", "RIGHTSHIFT", "RIGHTSHIFTEQUAL", "RPAR", "RSQB", "SEMI",
  "SLASH", "SLASHEQUAL", "STAR", "STAREQUAL", "STRING", "TILDE", "TRY",
  "VBAREQUAL", "WHILE", "WITH", "YIELD", "NAME", "FLOAT", "INT", "LESS",
  "GREATER", "GRLT", "GREATEREQUAL", "LESSEQUAL", "NOTEQUAL", "EQEQUAL",
  "IN", "NOT_IN", "IS", "IS_NOT", "$accept", "start", "file_input",
  "pick_NEWLINE_stmt", "star_NEWLINE_stmt", "decorator", "opt_arglist",
  "decorators", "decorated", "funcdef", "parameters", "varargslist",
  "opt_EQUAL_test", "star_fpdef_COMMA", "opt_DOUBLESTAR_NAME",
  "pick_STAR_DOUBLESTAR", "opt_COMMA", "fpdef", "fplist",
  "star_fpdef_notest", "stmt", "simple_stmt", "star_SEMI_small_stmt",
  "small_stmt", "expr_stmt", "pick_yield_expr_testlist", "star_EQUAL",
  "augassign", "print_stmt", "star_COMMA_test", "opt_test",
  "plus_COMMA_test", "opt_test_2", "del_stmt", "pass_stmt", "flow_stmt",
  "break_stmt", "continue_stmt", "return_stmt", "yield_stmt", "raise_stmt",
  "opt_COMMA_test", "opt_test_3", "import_stmt", "import_name",
  "import_from", "pick_dotted_name", "pick_STAR_import", "import_as_name",
  "dotted_as_name", "import_as_names", "star_COMMA_import_as_name",
  "dotted_as_names", "dotted_name", "global_stmt", "star_COMMA_NAME",
  "exec_stmt", "assert_stmt", "compound_stmt", "if_stmt", "star_ELIF",
  "while_stmt", "for_stmt", "try_stmt", "plus_except", "opt_ELSE",
  "opt_FINALLY", "with_stmt", "star_COMMA_with_item", "with_item",
  "except_clause", "pick_AS_COMMA", "opt_AS_COMMA", "suite", "plus_stmt",
  "testlist_safe", "plus_COMMA_old_test", "old_test", "old_lambdef",
  "test", "opt_IF_ELSE", "or_test", "and_test", "not_test", "comparison",
  "comp_op", "expr", "xor_expr", "and_expr", "shift_expr",
  "pick_LEFTSHIFT_RIGHTSHIFT", "arith_expr", "pick_PLUS_MINUS", "term",
  "pick_multop", "factor", "pick_unop", "power", "star_trailer", "atom",
  "pick_yield_expr_testlist_comp", "opt_yield_test", "opt_listmaker",
  "opt_dictorsetmaker", "plus_STRING", "listmaker", "testlist_comp",
  "lambdef", "trailer", "subscriptlist", "star_COMMA_subscript",
  "subscript", "opt_test_only", "opt_sliceop", "sliceop", "exprlist",
  "star_COMMA_expr", "testlist", "dictorsetmaker", "star_test_COLON_test",
  "pick_for_test_test", "pick_for_test", "classdef", "opt_testlist",
  "arglist", "star_argument_COMMA", "star_COMMA_argument",
  "opt_DOUBLESTAR_test", "pick_argument", "argument", "opt_comp_for",
  "list_iter", "list_for", "list_if", "comp_iter", "comp_for", "comp_if",
  "testlist1", "yield_expr", "star_DOT", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343
};
# endif

#define YYPACT_NINF -398

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-398)))

#define YYTABLE_NINF -261

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    -398,    43,  -398,   421,  -398,   342,   -26,   342,  -398,   -22,
    -398,   -17,   187,  -398,   187,   187,  -398,    22,   342,   -26,
      48,   342,    93,   342,  -398,  -398,   793,  -398,  -398,   245,
     342,   342,  -398,  -398,    74,   342,   342,   342,  -398,  -398,
    -398,  -398,  -398,   157,  -398,  -398,  -398,  -398,  -398,  -398,
    -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,
    -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,
    -398,  -398,    58,   121,  -398,    69,   133,   145,   159,    -3,
      42,    20,  -398,   187,  -398,  -398,    98,  -398,   753,  -398,
    -398,   156,  -398,    27,  -398,   118,    29,   130,   133,  -398,
      11,    90,   142,    12,  -398,   168,  -398,   175,    87,   342,
     178,    16,    32,   137,  -398,   164,  -398,   138,  -398,  -398,
     165,   139,  -398,  -398,   342,  -398,  -398,   184,  -398,   634,
     188,  -398,   198,  -398,  -398,  -398,  -398,    15,   192,   793,
     793,  -398,   793,   125,  -398,  -398,  -398,  -398,  -398,  -398,
    -398,  -398,   163,   187,   187,   187,   187,  -398,  -398,   187,
    -398,  -398,   187,  -398,  -398,  -398,  -398,   187,  -398,    77,
    -398,  -398,  -398,  -398,  -398,    93,  -398,  -398,  -398,  -398,
    -398,  -398,  -398,  -398,  -398,    93,   342,   141,   151,  -398,
    -398,   342,   634,   342,   160,   202,   206,   342,   342,    -9,
     181,   204,   207,   634,   -26,   154,  -398,   342,   161,    -6,
     166,  -398,  -398,   209,   342,   187,   223,  -398,  -398,  -398,
     223,  -398,  -398,   187,   223,  -398,  -398,   226,   223,   342,
    -398,   205,  -398,   112,   634,   162,   187,  -398,   684,   342,
      24,   121,  -398,  -398,  -398,   133,   145,   159,    -3,    42,
      20,  -398,   169,   187,   151,   331,  -398,   216,  -398,  -398,
    -398,  -398,  -398,   185,  -398,    37,  -398,  -398,  -398,   186,
    -398,   191,   634,   187,   233,   238,   182,  -398,   252,  -398,
    -398,  -398,   193,  -398,  -398,  -398,  -398,  -398,  -398,   200,
     243,   342,   251,   164,   189,   342,  -398,  -398,   194,  -398,
     342,   253,  -398,  -398,   233,   563,   342,   255,   108,   256,
     246,   634,   342,   133,  -398,  -398,  -398,   342,  -398,  -398,
     211,   257,   262,   218,  -398,   266,  -398,   235,   342,   342,
      95,  -398,   267,   272,  -398,  -398,   133,   342,  -398,   634,
     231,   219,   281,  -398,    84,   292,  -398,   285,  -398,  -398,
      14,  -398,   294,  -398,  -398,   793,   782,  -398,   342,  -398,
    -398,  -398,   492,    44,   634,   300,   284,   302,   634,   303,
    -398,  -398,  -398,  -398,   298,  -398,   310,   342,  -398,  -398,
    -398,   342,  -398,  -398,   270,  -398,   634,  -398,   308,  -398,
    -398,   182,   342,   312,    -6,   264,   342,  -398,    19,   322,
      89,   325,  -398,   293,  -398,  -398,  -398,  -398,  -398,   342,
    -398,  -398,   634,   329,  -398,   634,  -398,   634,  -398,   290,
    -398,   330,   332,  -398,  -398,   334,  -398,   335,   634,  -398,
    -398,   339,   782,  -398,  -398,  -398,   782,   340,   782,  -398,
    -398,  -398,   782,   341,  -398,  -398,   634,  -398,  -398,  -398,
     342,  -398,  -398,   724,  -398,   634,   634,  -398,   342,   122,
    -398,   782,    89,  -398,   782,  -398,  -398,  -398,   342,  -398,
    -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       7,     0,     2,     0,     1,     0,     0,     0,    87,     0,
      88,     0,     0,     3,     0,     0,   314,     0,     0,     0,
      24,   240,   236,   238,   219,     4,     0,    81,   218,    75,
      93,    90,   242,   220,     0,     0,     0,   312,   229,   230,
     231,     6,    13,     0,   133,   131,     5,    37,    42,    43,
      44,    45,    46,    47,    82,    83,    84,    86,    85,    48,
      98,    99,    49,    50,    51,    38,   126,   127,   128,   129,
     130,    73,   176,   177,   179,   182,   183,   196,   198,   200,
     202,   206,   210,     0,   217,   224,   232,   174,    57,   132,
      91,   125,   117,     0,   309,     0,     0,     0,   268,    80,
     123,     0,     0,     0,   121,     0,   115,   100,   110,     0,
       0,     0,    73,     0,   239,    73,   235,     0,   234,   233,
      73,     0,   237,   181,     0,    70,    73,    97,    89,     0,
       0,   152,   154,   311,    12,    15,    14,     0,   270,     0,
       0,   173,     0,     0,   185,   186,   190,   188,   189,   191,
     187,   192,   194,     0,     0,     0,     0,   204,   205,     0,
     209,   208,     0,   215,   214,   213,   212,     0,   216,   222,
     241,    63,    65,    69,    68,     0,    66,    59,    62,    58,
      67,    61,    60,    64,    53,     0,     0,     0,   285,     9,
     228,     0,     0,   282,    24,     0,   266,     0,     0,     0,
     313,   102,   119,     0,     0,     0,   248,     0,     0,     0,
       0,    31,    19,    22,     0,     0,    30,   272,   277,   227,
      30,   245,   225,     0,    30,   243,   226,    79,    30,     0,
      92,     0,   161,     0,     0,     0,     0,    40,     0,   269,
       0,   178,   180,   193,   195,   184,   197,   199,   201,   203,
     207,   211,     0,     0,   285,   260,   223,    57,    55,    54,
      52,   124,   118,     0,    10,     0,   310,   280,   281,     0,
      18,     0,     0,   265,    95,     0,     0,   104,   108,   101,
     114,   106,     0,   137,   116,   109,   247,    28,    36,     0,
      26,     0,    30,   274,     0,    29,   278,   246,     0,   244,
       0,    30,    71,    74,    95,     0,   156,     0,   147,     0,
     139,     0,     0,   153,    39,    41,    72,     0,   251,   221,
       0,     0,   257,     0,   255,     0,    56,     0,     0,     0,
     296,   283,    30,     0,    17,    16,   267,     0,   122,     0,
       0,     0,   112,   120,   135,    34,    32,     0,    27,    21,
      23,    20,    30,   271,   275,     0,     0,    77,    29,    78,
      96,   164,     0,   160,     0,     0,   149,     0,     0,     0,
     150,   151,   175,   249,     0,   250,   253,   260,     8,   292,
     287,     0,   293,   295,   284,   290,     0,    94,   141,   105,
     107,   111,     0,     0,    33,     0,    29,   276,   306,    24,
     300,   166,   170,   169,    76,   162,   163,   157,   158,     0,
     155,   143,     0,     0,   142,     0,   145,     0,   256,   252,
     259,   262,   289,   294,   279,     0,   113,     0,     0,    35,
      25,     0,     0,   305,   303,   304,     0,     0,     0,   299,
     297,   298,     0,    30,   159,   146,     0,   144,   138,   254,
     264,   258,   261,     0,   291,     0,     0,   134,     0,   308,
     172,     0,   302,   168,    29,   165,   148,   263,     0,   286,
     140,   136,   273,   307,   171,   301,   167,   288
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -398,  -398,  -398,  -398,  -398,   304,    92,  -398,  -398,   313,
    -398,  -185,  -398,  -398,  -398,  -398,  -192,  -199,  -398,  -398,
    -283,    -2,  -398,   123,  -398,   177,   106,  -398,  -398,     3,
    -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,
    -398,    60,  -398,  -398,  -398,  -398,  -398,  -398,   -23,   174,
      97,  -398,  -398,    13,  -398,  -398,  -398,  -398,  -398,  -398,
    -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,    57,
      72,  -398,  -398,  -180,  -398,  -398,  -398,  -397,  -398,    -5,
    -398,  -136,   249,   -12,  -398,  -398,    -8,   229,   237,   240,
    -398,   234,  -398,   232,  -398,   -68,  -398,  -398,  -398,  -398,
    -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,  -398,
    -398,   -21,    25,  -398,  -398,   -10,  -398,   -11,  -398,  -398,
    -398,  -398,   354,  -398,  -398,  -398,  -398,  -398,  -398,   -52,
    -398,   -59,   289,  -398,   -54,  -104,  -398,  -398,     5,  -398
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,    41,     3,    42,   263,    43,    44,    45,
     195,   110,   292,   111,   348,   212,   296,   213,   289,   345,
      46,   232,   137,    48,    49,   257,   184,   185,    50,   138,
     125,   301,   302,    51,    52,    53,    54,    55,    56,    57,
      58,   338,   230,    59,    60,    61,   102,   279,   280,   106,
     281,   342,   107,   108,    62,   202,    63,    64,    65,    66,
     344,    67,    68,    69,   308,   366,   414,    70,   235,   131,
     309,   409,   410,   233,   362,   400,   443,   401,   402,    71,
     141,    72,    73,    74,    75,   153,    76,    77,    78,    79,
     159,    80,   162,    81,   167,    82,    83,    84,   169,    85,
     116,   117,   121,   113,    86,   122,   118,    87,   256,   323,
     376,   324,   325,   451,   452,    99,   196,    88,   114,   352,
     353,   217,    89,   269,   264,   265,   422,   454,   331,   332,
     382,   439,   440,   441,   433,   434,   435,    95,    90,   103
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      91,    47,    94,   240,    98,   101,   100,    98,   218,   271,
     288,   221,   267,   105,   123,   168,   112,   115,   120,    93,
     128,   154,   361,   283,   126,   127,   133,   119,   297,   -29,
     130,   132,   299,   200,   276,   459,   303,   209,   157,   460,
     208,   462,   163,     4,   192,   463,     7,   214,   187,    92,
     407,   317,   215,    96,   310,   432,   158,   277,    97,   209,
     408,   328,   237,   109,   474,   215,   278,   476,   140,   211,
     188,   164,   193,   140,   189,   -29,    20,    21,   238,   406,
      22,    23,   210,    24,   165,    26,   166,    92,   160,   129,
      28,   211,   335,   205,   139,   161,   197,   104,   252,   251,
     351,   253,     7,   329,   206,    32,    33,   140,   187,   359,
     392,   393,    38,    39,    40,   216,   201,   143,   220,   227,
     254,   255,   223,   224,   381,   438,   142,   190,   215,   228,
     242,   370,    20,    21,   191,   365,    22,    23,   306,    24,
     385,    26,   306,   154,   307,   245,    28,   144,   145,   146,
     147,   148,   149,   150,   151,   215,   152,   155,   432,   388,
     397,    32,    33,   156,   258,     6,   170,    37,    38,    39,
      40,     9,   186,   194,   258,   198,    11,   311,   312,   199,
     259,   261,   268,   203,   411,   319,   266,   275,   416,   354,
     259,   204,   274,   207,   219,   429,     7,   215,   223,   222,
     229,   226,   286,   234,   236,   294,   424,    98,   239,   293,
     243,   244,   -11,   298,   437,    98,   262,   272,  -103,   398,
     403,   270,   273,   282,   304,   187,   383,    21,   313,   285,
      22,    23,   445,    24,   316,   447,   287,   448,   291,   295,
      28,   290,   300,   305,   318,   175,   327,   333,   457,   337,
     322,   465,   334,   339,     7,    32,    33,   278,   341,   347,
     330,   346,    38,    39,    40,   336,   466,   350,   343,   358,
     364,   368,   373,   369,   355,   470,   471,  -259,   374,   356,
     375,   377,   378,   384,    20,    21,   349,   386,    22,    23,
     316,    24,   389,    26,   390,   357,   403,   391,    28,     7,
     403,   363,   403,    47,   124,  -260,   403,   132,   394,   395,
     396,   321,   372,    32,    33,   412,   413,   415,   417,   418,
      38,    39,    40,   379,   380,   403,   419,   428,   403,    20,
      21,   -29,   387,    22,    23,   425,    24,   436,    26,   430,
       7,   442,   140,    28,   446,   450,   320,   134,   453,   455,
     456,     7,   321,   404,   458,   461,   135,   464,    32,    33,
      47,   315,   260,   326,   360,    38,    39,    40,   426,   371,
      20,    21,   420,   340,    22,    23,   423,    24,   284,    26,
     367,    20,    21,   246,    28,    22,    23,   427,    24,   241,
      26,   431,   247,   249,   250,    28,   248,   136,   449,    32,
      33,   469,   421,   475,   444,   473,    38,    39,    40,   225,
      32,    33,     0,     0,   322,     0,     0,    38,    39,    40,
       0,     0,     0,     0,     0,     0,     0,     0,     5,     6,
       7,     0,     8,     0,     0,     9,     0,     0,    10,     0,
      11,    12,     0,     0,     0,   467,     0,     0,   330,    13,
       0,     0,    14,   472,    15,    16,    17,    18,    19,     0,
      20,    21,     0,   477,    22,    23,     0,    24,    25,    26,
       0,    27,     0,     0,    28,     0,    29,    30,     0,    31,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    32,
      33,    34,     0,    35,    36,    37,    38,    39,    40,     5,
       6,     7,     0,     8,     0,     0,     9,     0,     0,    10,
     405,    11,    12,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    14,     0,    15,    16,    17,    18,    19,
       0,    20,    21,     0,     0,    22,    23,     0,    24,     0,
      26,     0,    27,     0,     0,    28,     0,    29,    30,     0,
      31,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      32,    33,    34,     0,    35,    36,    37,    38,    39,    40,
       5,     6,     7,     0,     8,     0,     0,     9,     0,     0,
      10,     0,    11,    12,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    14,     0,    15,    16,    17,    18,
      19,     0,    20,    21,     0,     0,    22,    23,     0,    24,
       0,    26,     0,    27,     0,     0,    28,     0,    29,    30,
       0,    31,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    32,    33,    34,     0,    35,    36,    37,    38,    39,
      40,     5,     0,     7,     0,     8,     0,     0,     0,     0,
       0,    10,     0,     0,    12,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    14,     0,     0,    16,    17,
       0,    19,     0,    20,    21,     0,     0,    22,    23,     0,
      24,   231,    26,     0,    27,     0,     0,    28,     0,    29,
      30,     5,    31,     7,     0,     8,     0,     0,     0,     0,
       0,    10,    32,    33,    12,     0,     0,     0,    37,    38,
      39,    40,     0,     0,     0,    14,     0,     0,    16,    17,
       0,    19,     0,    20,    21,     0,     0,    22,    23,     0,
      24,   314,    26,     7,    27,     0,     0,    28,     0,    29,
      30,     0,    31,     0,     0,     0,     0,     0,   468,     0,
       0,     0,    32,    33,     0,     0,   171,     0,    37,    38,
      39,    40,     0,    20,    21,     0,   172,    22,    23,     0,
      24,     0,    26,     0,     0,     0,   173,    28,   174,     0,
       0,     0,   175,     0,     0,     0,     0,     0,     0,     0,
       0,     7,    32,    33,     0,   176,     0,     0,   177,    38,
      39,    40,     7,     0,     0,   178,     0,   179,     0,     0,
       0,     0,     0,   180,     0,     0,     0,     0,   181,     0,
     182,   399,    21,     0,   183,    22,    23,     0,    24,     0,
      26,     0,     0,    21,     0,    28,    22,    23,     0,    24,
       0,    26,     0,     0,     0,     0,    28,     0,     0,     0,
      32,    33,     0,     0,     0,     0,     0,    38,    39,    40,
       0,    32,    33,     0,     0,     0,     0,     0,    38,    39,
      40
};

static const yytype_int16 yycheck[] =
{
       5,     3,     7,   139,    12,    15,    14,    15,   112,   194,
     209,   115,   192,    18,    26,    83,    21,    22,    23,     6,
      31,    10,   305,   203,    29,    30,    37,    22,   220,    15,
      35,    36,   224,    21,    43,   432,   228,    43,    41,   436,
      24,   438,    22,     0,    15,   442,     9,    15,    21,    75,
       6,    27,    33,    75,   234,    36,    59,    66,    75,    43,
      16,    24,    47,    15,   461,    33,    75,   464,    49,    75,
      43,    51,    43,    49,    47,    61,    39,    40,    63,   362,
      43,    44,    66,    46,    64,    48,    66,    75,    46,    15,
      53,    75,   272,     6,    36,    53,    85,    75,    21,   167,
     292,    24,     9,    66,   109,    68,    69,    49,    21,   301,
      26,    27,    75,    76,    77,   112,   103,    48,   115,   124,
      43,    44,    33,   120,    29,    36,     5,     9,    33,   126,
     142,   311,    39,    40,    16,    27,    43,    44,    30,    46,
     332,    48,    30,    10,    32,   153,    53,    78,    79,    80,
      81,    82,    83,    84,    85,    33,    87,    12,    36,   339,
     352,    68,    69,     4,   175,     8,    68,    74,    75,    76,
      77,    14,    16,    43,   185,    85,    19,    15,    16,    37,
     175,   186,   193,    15,   364,   253,   191,   198,   368,   293,
     185,    16,   197,    15,    57,   394,     9,    33,    33,    61,
      16,    62,   207,    15,     6,   215,   386,   215,    16,   214,
      85,    48,    61,   223,   399,   223,    75,    15,    37,   355,
     356,    61,    16,    16,   229,    21,   330,    40,   236,    75,
      43,    44,   412,    46,   239,   415,    75,   417,    29,    16,
      53,    75,    16,    38,    75,    29,    61,    61,   428,    16,
     255,   443,    61,    15,     9,    68,    69,    75,     6,    16,
     265,    61,    75,    76,    77,   273,   446,    16,    75,    16,
      15,    15,    61,    27,    85,   455,   456,    15,    21,    85,
      62,    15,    47,    16,    39,    40,   291,    15,    43,    44,
     295,    46,    61,    48,    75,   300,   432,    16,    53,     9,
     436,   306,   438,   305,    59,    15,   442,   312,    16,    24,
      16,    21,   317,    68,    69,    15,    32,    15,    15,    21,
      75,    76,    77,   328,   329,   461,    16,    15,   464,    39,
      40,    61,   337,    43,    44,    27,    46,    15,    48,    75,
       9,    16,    49,    53,    15,    15,   254,    43,    16,    15,
      15,     9,    21,   358,    15,    15,    43,    16,    68,    69,
     362,   238,   185,   257,   304,    75,    76,    77,   391,   312,
      39,    40,   377,   276,    43,    44,   381,    46,   204,    48,
     308,    39,    40,   154,    53,    43,    44,   392,    46,   140,
      48,   396,   155,   159,   162,    53,   156,    43,   419,    68,
      69,   453,   377,   462,   409,   459,    75,    76,    77,   120,
      68,    69,    -1,    -1,   419,    -1,    -1,    75,    76,    77,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     7,     8,
       9,    -1,    11,    -1,    -1,    14,    -1,    -1,    17,    -1,
      19,    20,    -1,    -1,    -1,   450,    -1,    -1,   453,    28,
      -1,    -1,    31,   458,    33,    34,    35,    36,    37,    -1,
      39,    40,    -1,   468,    43,    44,    -1,    46,    47,    48,
      -1,    50,    -1,    -1,    53,    -1,    55,    56,    -1,    58,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,
      69,    70,    -1,    72,    73,    74,    75,    76,    77,     7,
       8,     9,    -1,    11,    -1,    -1,    14,    -1,    -1,    17,
      18,    19,    20,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    31,    -1,    33,    34,    35,    36,    37,
      -1,    39,    40,    -1,    -1,    43,    44,    -1,    46,    -1,
      48,    -1,    50,    -1,    -1,    53,    -1,    55,    56,    -1,
      58,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      68,    69,    70,    -1,    72,    73,    74,    75,    76,    77,
       7,     8,     9,    -1,    11,    -1,    -1,    14,    -1,    -1,
      17,    -1,    19,    20,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    31,    -1,    33,    34,    35,    36,
      37,    -1,    39,    40,    -1,    -1,    43,    44,    -1,    46,
      -1,    48,    -1,    50,    -1,    -1,    53,    -1,    55,    56,
      -1,    58,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    69,    70,    -1,    72,    73,    74,    75,    76,
      77,     7,    -1,     9,    -1,    11,    -1,    -1,    -1,    -1,
      -1,    17,    -1,    -1,    20,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    31,    -1,    -1,    34,    35,
      -1,    37,    -1,    39,    40,    -1,    -1,    43,    44,    -1,
      46,    47,    48,    -1,    50,    -1,    -1,    53,    -1,    55,
      56,     7,    58,     9,    -1,    11,    -1,    -1,    -1,    -1,
      -1,    17,    68,    69,    20,    -1,    -1,    -1,    74,    75,
      76,    77,    -1,    -1,    -1,    31,    -1,    -1,    34,    35,
      -1,    37,    -1,    39,    40,    -1,    -1,    43,    44,    -1,
      46,    47,    48,     9,    50,    -1,    -1,    53,    -1,    55,
      56,    -1,    58,    -1,    -1,    -1,    -1,    -1,    24,    -1,
      -1,    -1,    68,    69,    -1,    -1,     3,    -1,    74,    75,
      76,    77,    -1,    39,    40,    -1,    13,    43,    44,    -1,
      46,    -1,    48,    -1,    -1,    -1,    23,    53,    25,    -1,
      -1,    -1,    29,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     9,    68,    69,    -1,    42,    -1,    -1,    45,    75,
      76,    77,     9,    -1,    -1,    52,    -1,    54,    -1,    -1,
      -1,    -1,    -1,    60,    -1,    -1,    -1,    -1,    65,    -1,
      67,    39,    40,    -1,    71,    43,    44,    -1,    46,    -1,
      48,    -1,    -1,    40,    -1,    53,    43,    44,    -1,    46,
      -1,    48,    -1,    -1,    -1,    -1,    53,    -1,    -1,    -1,
      68,    69,    -1,    -1,    -1,    -1,    -1,    75,    76,    77,
      -1,    68,    69,    -1,    -1,    -1,    -1,    -1,    75,    76,
      77
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    90,    91,    93,     0,     7,     8,     9,    11,    14,
      17,    19,    20,    28,    31,    33,    34,    35,    36,    37,
      39,    40,    43,    44,    46,    47,    48,    50,    53,    55,
      56,    58,    68,    69,    70,    72,    73,    74,    75,    76,
      77,    92,    94,    96,    97,    98,   109,   110,   112,   113,
     117,   122,   123,   124,   125,   126,   127,   128,   129,   132,
     133,   134,   143,   145,   146,   147,   148,   150,   151,   152,
     156,   168,   170,   171,   172,   173,   175,   176,   177,   178,
     180,   182,   184,   185,   186,   188,   193,   196,   206,   211,
     227,   168,    75,   142,   168,   226,    75,    75,   175,   204,
     175,   204,   135,   228,    75,   168,   138,   141,   142,    15,
     100,   102,   168,   192,   207,   168,   189,   190,   195,   227,
     168,   191,   194,   172,    59,   119,   168,   168,   206,    15,
     168,   158,   168,   206,    94,    98,   211,   111,   118,    36,
      49,   169,     5,    48,    78,    79,    80,    81,    82,    83,
      84,    85,    87,   174,    10,    12,     4,    41,    59,   179,
      46,    53,   181,    22,    51,    64,    66,   183,   184,   187,
      68,     3,    13,    23,    25,    29,    42,    45,    52,    54,
      60,    65,    67,    71,   115,   116,    16,    21,    43,    47,
       9,    16,    15,    43,    43,    99,   205,    85,    85,    37,
      21,   142,   144,    15,    16,     6,   168,    15,    24,    43,
      66,    75,   104,   106,    15,    33,   118,   210,   224,    57,
     118,   224,    61,    33,   118,   221,    62,   168,   118,    16,
     131,    47,   110,   162,    15,   157,     6,    47,    63,    16,
     170,   171,   172,    85,    48,   175,   176,   177,   178,   180,
     182,   184,    21,    24,    43,    44,   197,   114,   206,   227,
     114,   168,    75,    95,   213,   214,   168,   162,   206,   212,
      61,   100,    15,    16,   168,   206,    43,    66,    75,   136,
     137,   139,    16,   162,   138,    75,   168,    75,   106,   107,
      75,    29,   101,   168,   204,    16,   105,   105,   204,   105,
      16,   120,   121,   105,   168,    38,    30,    32,   153,   159,
     162,    15,    16,   175,    47,   112,   168,    27,    75,   184,
      95,    21,   168,   198,   200,   201,   115,    61,    24,    66,
     168,   217,   218,    61,    61,   162,   175,    16,   130,    15,
     139,     6,   140,    75,   149,   108,    61,    16,   103,   168,
      16,   105,   208,   209,   224,    85,    85,   168,    16,   105,
     130,   109,   163,   168,    15,    27,   154,   159,    15,    27,
     162,   158,   168,    61,    21,    62,   199,    15,    47,   168,
     168,    29,   219,   224,    16,   105,    15,   168,   162,    61,
      75,    16,    26,    27,    16,    24,    16,   105,   170,    39,
     164,   166,   167,   170,   168,    18,   109,     6,    16,   160,
     161,   162,    15,    32,   155,    15,   162,    15,    21,    16,
     168,   201,   215,   168,   162,    27,   137,   168,    15,   106,
      75,   168,    36,   223,   224,   225,    15,   100,    36,   220,
     221,   222,    16,   165,   168,   162,    15,   162,   162,   200,
      15,   202,   203,    16,   216,    15,    15,   162,    15,   166,
     166,    15,   166,   166,    16,   105,   162,   168,    24,   218,
     162,   162,   168,   223,   166,   220,   166,   168
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    89,    90,    91,    92,    92,    93,    93,    94,    94,
      95,    95,    96,    96,    97,    97,    98,    99,    99,   100,
     100,   101,   101,   102,   102,   103,   103,   104,   104,   105,
     105,   106,   106,   107,   107,   108,   108,   109,   109,   110,
     110,   111,   111,   112,   112,   112,   112,   112,   112,   112,
     112,   112,   113,   113,   114,   114,   115,   115,   116,   116,
     116,   116,   116,   116,   116,   116,   116,   116,   116,   116,
     117,   117,   118,   118,   119,   119,   120,   120,   121,   121,
     122,   123,   124,   124,   124,   124,   124,   125,   126,   127,
     127,   128,   129,   129,   130,   130,   131,   131,   132,   132,
     133,   134,   135,   135,   136,   136,   136,   137,   137,   138,
     138,   139,   139,   140,   140,   141,   141,   142,   142,   143,
     144,   144,   145,   145,   146,   146,   147,   147,   147,   147,
     147,   147,   147,   147,   148,   148,   149,   149,   150,   150,
     151,   151,   152,   152,   153,   153,   154,   154,   155,   155,
     156,   157,   157,   158,   158,   159,   159,   160,   160,   161,
     161,   162,   162,   163,   163,   164,   164,   165,   165,   166,
     166,   167,   167,   168,   168,   169,   169,   170,   170,   171,
     171,   172,   172,   173,   173,   174,   174,   174,   174,   174,
     174,   174,   174,   174,   174,   174,   175,   175,   176,   176,
     177,   177,   178,   178,   179,   179,   180,   180,   181,   181,
     182,   182,   183,   183,   183,   183,   184,   184,   185,   185,
     185,   186,   186,   187,   187,   188,   188,   188,   188,   188,
     188,   188,   188,   189,   189,   190,   190,   191,   191,   192,
     192,   193,   193,   194,   194,   195,   195,   196,   196,   197,
     197,   197,   198,   198,   199,   199,   200,   200,   200,   201,
     201,   202,   202,   203,   203,   204,   204,   205,   205,   206,
     206,   207,   207,   208,   208,   209,   209,   210,   210,   211,
     211,   212,   212,   213,   214,   214,   215,   215,   216,   216,
     217,   217,   217,   218,   218,   219,   219,   220,   220,   221,
     221,   222,   222,   223,   223,   224,   224,   225,   225,   226,
     226,   227,   227,   228,   228
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     1,     1,     2,     0,     6,     3,
       1,     0,     2,     1,     2,     2,     5,     3,     2,     2,
       4,     2,     0,     4,     0,     3,     0,     3,     2,     1,
       0,     1,     3,     3,     2,     3,     0,     1,     1,     4,
       3,     3,     0,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     2,     1,     1,     3,     0,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       2,     4,     3,     0,     3,     0,     3,     2,     2,     0,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     2,
       1,     1,     3,     1,     2,     0,     3,     0,     1,     1,
       2,     4,     2,     2,     1,     3,     1,     3,     1,     3,
       1,     3,     2,     3,     0,     1,     3,     1,     3,     3,
       3,     0,     5,     2,     4,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     8,     5,     5,     0,     7,     4,
       9,     6,     6,     6,     4,     3,     3,     0,     3,     0,
       5,     3,     0,     3,     1,     3,     1,     1,     1,     2,
       0,     1,     4,     2,     1,     3,     1,     3,     2,     1,
       1,     4,     3,     2,     1,     4,     0,     1,     3,     1,
       3,     2,     1,     1,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     1,     2,     1,     3,     1,     3,
       1,     3,     1,     3,     1,     1,     1,     3,     1,     1,
       1,     3,     1,     1,     1,     1,     2,     1,     1,     1,
       1,     4,     2,     2,     0,     3,     3,     3,     3,     1,
       1,     1,     1,     1,     1,     1,     0,     1,     0,     1,
       0,     2,     1,     2,     3,     2,     3,     4,     3,     3,
       3,     2,     3,     2,     3,     0,     3,     1,     4,     1,
       0,     1,     0,     2,     1,     3,     2,     3,     0,     3,
       2,     4,     2,     5,     0,     1,     2,     1,     2,     7,
       4,     1,     0,     2,     3,     0,     3,     0,     3,     0,
       2,     4,     2,     2,     3,     1,     0,     1,     1,     5,
       4,     3,     2,     1,     1,     5,     4,     3,     2,     1,
       3,     2,     1,     2,     0
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Location data for the lookahead symbol.  */
YYLTYPE yylloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 5:
#line 67 "includes/parse.y" /* yacc.c:1646  */
    {
        if ((yyvsp[0].node) != NULL)
        {
            (yyvsp[0].node)->eval();
        }
        else
        {
            throw std::string("unsupported operation!");
        }
    }
#line 1914 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 88 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = (yyvsp[0].vec);
    }
#line 1922 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 92 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = NULL;
    }
#line 1930 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 106 "includes/parse.y" /* yacc.c:1646  */
    {
        ParametersNode* pnode = new ParametersNode((yyvsp[-2].vec));
        SuiteNode* snode = new SuiteNode((yyvsp[0].vec));
        Node* funcNode = new FuncNode((yyvsp[-3].id), pnode, snode);
        (yyval.node) = funcNode;
        pool.add(pnode);
        pool.add(snode);
        pool.add(funcNode);
        delete [] (yyvsp[-3].id);
    }
#line 1945 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 119 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = (yyvsp[-1].vec);
    }
#line 1953 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 123 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = new std::vector<Node*>();
    }
#line 1961 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 129 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = NULL;
    }
#line 1969 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 133 "includes/parse.y" /* yacc.c:1646  */
    {
        if ((yyvsp[-3].vec) == NULL)
        {
            (yyvsp[-3].vec) = new std::vector<Node*>();
        }

        Node* node = NULL;
        if ((yyvsp[-1].node) != NULL)
        {
            node = new AsgBinaryNode((yyvsp[-2].node), (yyvsp[-1].node));
            pool.add(node);
        }
        else
        {
            node = (yyvsp[-2].node);
        }

        (yyvsp[-3].vec)->push_back(node);
        (yyval.vec) = (yyvsp[-3].vec);
    }
#line 1994 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 156 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[0].node);
    }
#line 2002 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 160 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2010 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 166 "includes/parse.y" /* yacc.c:1646  */
    {
        if ((yyvsp[-3].vec) == NULL)
        {
            (yyvsp[-3].vec) = new std::vector<Node*>();
        }
        
        Node* node = NULL;
        if ((yyvsp[-1].node) != NULL)
        {
            node = new AsgBinaryNode((yyvsp[-2].node), (yyvsp[-1].node));
            pool.add(node);
        }
        else
        {
            node = (yyvsp[-2].node);
        }

        (yyvsp[-3].vec)->push_back(node);
        (yyval.vec) = (yyvsp[-3].vec);
    }
#line 2035 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 187 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = NULL;
    }
#line 2043 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 192 "includes/parse.y" /* yacc.c:1646  */
    { delete [] (yyvsp[0].id); }
#line 2049 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 196 "includes/parse.y" /* yacc.c:1646  */
    { delete [] (yyvsp[-1].id); }
#line 2055 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 197 "includes/parse.y" /* yacc.c:1646  */
    { delete [] (yyvsp[0].id); }
#line 2061 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 205 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = new IdentNode((yyvsp[0].id));

        pool.add((yyval.node));
        delete[] (yyvsp[0].id);
    }
#line 2072 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 212 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2080 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 226 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[0].node);
    }
#line 2088 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 230 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[0].node);
    }
#line 2096 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 236 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[-3].node);
    }
#line 2104 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 240 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[-2].node);
    }
#line 2112 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 246 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2120 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 250 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2128 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 256 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[0].node);
    }
#line 2136 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 260 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[0].node);
    }
#line 2144 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 264 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2152 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 268 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2160 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 272 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[0].node);
    }
#line 2168 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 276 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2176 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 280 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[0].node);
    }
#line 2184 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 284 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2192 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 288 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2200 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 294 "includes/parse.y" /* yacc.c:1646  */
    {
        /* 1 +=, 2 -=, 3 *=,  4 /= 
		 * 5 %=, 6 &=, 7 |=,  8 ^=
		 * 9 <<=, 10 >>=, 11, **= 12 //=
		 */
		Node* node = NULL;
		switch((yyvsp[-1].intNumber))
		{
			case 1:	/* += */
					node = new AddEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 2: /* -= */
					node = new SubEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 3: /* *= */
					node = new MulEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 4: /* /= */
					node = new DivEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 5: /* %= */
					node = new PercentEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 6: /* &= */
					node = new AmpersandEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 7: /* |= */
					node = new BarEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 8: /* ^= */
					node = new CircumflexEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 9: /* <<= */
					node = new LeftShiftEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 10: /* >>= */
					node = new RightShiftEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 11: /* **= */
					 node = new PowerEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
			case 12: /* //= */
					 node = new FloorDivEqualNode((yyvsp[-2].node), (yyvsp[0].node)); break;
		}
		(yyval.node) = node;
		pool.add((yyval.node));
	}
#line 2241 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 331 "includes/parse.y" /* yacc.c:1646  */
    {
		const IdentNode* ptr = dynamic_cast<const IdentNode*>((yyvsp[-1].node));

		if (ptr == NULL && (yyvsp[0].node) != NULL)
		{
			throw std::string("wrong assign operation");
		}
		if ((yyvsp[0].node) != NULL)
		{
			(yyval.node) = new AsgBinaryNode((yyvsp[-1].node), (yyvsp[0].node));
			pool.add((yyval.node));
		}
		else
		{
			(yyval.node) = (yyvsp[-1].node); //$1->eval()->print();
		}
	}
#line 2263 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 350 "includes/parse.y" /* yacc.c:1646  */
    { throw std::string("unsupported operations"); }
#line 2269 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 352 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2277 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 358 "includes/parse.y" /* yacc.c:1646  */
    {
		if ((yyvsp[0].node) == NULL)
		{
			(yyval.node) = (yyvsp[-1].node);
		}
		else
		{
			const IdentNode* ptr = dynamic_cast<const IdentNode*>((yyvsp[-1].node));

			if (ptr == NULL && (yyvsp[0].node) != NULL)
			{
				throw std::string("wrong assign operation");
			}
			if ((yyvsp[0].node) != NULL)
			{
				(yyval.node) = new AsgBinaryNode((yyvsp[-1].node), (yyvsp[0].node));
				pool.add((yyval.node));
			}
		}
	}
#line 2302 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 378 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2308 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 382 "includes/parse.y" /* yacc.c:1646  */
    {
		/* += */
		(yyval.intNumber) = 1;
	}
#line 2317 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 387 "includes/parse.y" /* yacc.c:1646  */
    {
		// -=
		(yyval.intNumber) = 2;
	}
#line 2326 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 392 "includes/parse.y" /* yacc.c:1646  */
    {
		// *=
		(yyval.intNumber) = 3;
	}
#line 2335 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 397 "includes/parse.y" /* yacc.c:1646  */
    {
		// /=
		(yyval.intNumber) = 4;
	}
#line 2344 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 402 "includes/parse.y" /* yacc.c:1646  */
    {
		// %=
		(yyval.intNumber) = 5;
	}
#line 2353 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 407 "includes/parse.y" /* yacc.c:1646  */
    {
		// &=
		(yyval.intNumber) = 6;
	}
#line 2362 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 412 "includes/parse.y" /* yacc.c:1646  */
    {
		// |=
		(yyval.intNumber) = 7;
	}
#line 2371 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 417 "includes/parse.y" /* yacc.c:1646  */
    {
		// ^=
		(yyval.intNumber) = 8;
	}
#line 2380 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 422 "includes/parse.y" /* yacc.c:1646  */
    {
		// <<=
		(yyval.intNumber) = 9;
	}
#line 2389 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 427 "includes/parse.y" /* yacc.c:1646  */
    {
		// >>=
		(yyval.intNumber) = 10;
	}
#line 2398 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 432 "includes/parse.y" /* yacc.c:1646  */
    {
		// **=
		(yyval.intNumber) = 11;
	}
#line 2407 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 437 "includes/parse.y" /* yacc.c:1646  */
    {
		// //=
		(yyval.intNumber) = 12;
	}
#line 2416 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 444 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = new PrintNode((yyvsp[0].vec));
        pool.add((yyval.node));
	}
#line 2425 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 449 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2433 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 455 "includes/parse.y" /* yacc.c:1646  */
    {
        if ((yyvsp[-2].vec) == NULL) 
        {
            (yyvsp[-2].vec) = new std::vector<Node*>();
        }
        (yyval.vec) = (yyvsp[-2].vec);
        (yyval.vec)->push_back((yyvsp[0].node));
	}
#line 2446 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 464 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.vec) = NULL; }
#line 2452 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 468 "includes/parse.y" /* yacc.c:1646  */
    {
		if ((yyvsp[-1].vec) != NULL)
        {
            (yyval.vec) = (yyvsp[-1].vec);
        }
        else
        {
            (yyval.vec) = new std::vector<Node*>();
        }
        (yyval.vec)->push_back((yyvsp[-2].node));

	}
#line 2469 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 481 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.vec) = NULL; }
#line 2475 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 498 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2481 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 499 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2487 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 500 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 2493 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 501 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2499 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 502 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2505 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 512 "includes/parse.y" /* yacc.c:1646  */
    {
        IntLiteral* endScope = new IntLiteral(1);
        pool.add(endScope);
        (yyval.node) = new ReturnNode((yyvsp[0].node), endScope);
        pool.add((yyval.node));
    }
#line 2516 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 519 "includes/parse.y" /* yacc.c:1646  */
    {
        Node* ret = new NoneLiteral();
        IntLiteral* endScope = new IntLiteral(1);
        pool.add(ret);
        pool.add(endScope);
        (yyval.node) = new ReturnNode(ret, endScope);
        pool.add((yyval.node));
    }
#line 2529 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 563 "includes/parse.y" /* yacc.c:1646  */
    { delete[] (yyvsp[-2].id); delete [] (yyvsp[0].id); }
#line 2535 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 564 "includes/parse.y" /* yacc.c:1646  */
    { delete [] (yyvsp[0].id); }
#line 2541 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 567 "includes/parse.y" /* yacc.c:1646  */
    { delete[] (yyvsp[0].id);}
#line 2547 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 117:
#line 583 "includes/parse.y" /* yacc.c:1646  */
    { delete [] (yyvsp[0].id); }
#line 2553 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 118:
#line 584 "includes/parse.y" /* yacc.c:1646  */
    { delete [] (yyvsp[0].id); }
#line 2559 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 119:
#line 588 "includes/parse.y" /* yacc.c:1646  */
    {
        Node* node = new IdentNode((yyvsp[-1].id));
        pool.add(node);
        if ((yyvsp[0].vec) == NULL)
        {
            (yyvsp[0].vec) = new std::vector<Node*>();
        }
        (yyvsp[0].vec)->push_back(node);
        (yyval.node) = new GlobalNode((yyvsp[0].vec));
        pool.add((yyval.node));
        delete [] (yyvsp[-1].id); 
    }
#line 2576 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 120:
#line 603 "includes/parse.y" /* yacc.c:1646  */
    {
        Node* node = new IdentNode((yyvsp[0].id));
        pool.add(node);
        if ((yyvsp[-2].vec) == NULL)
        {
            (yyval.vec) = new std::vector<Node*>();
        }
        else
        {
            (yyval.vec) = (yyvsp[-2].vec);
        }
        (yyval.vec)->push_back(node);
 
        delete [] (yyvsp[0].id); 
    }
#line 2596 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 121:
#line 618 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.vec) = NULL; }
#line 2602 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 126:
#line 629 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 2608 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 127:
#line 630 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2614 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 128:
#line 631 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2620 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 129:
#line 632 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2626 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 130:
#line 633 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2632 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 131:
#line 634 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 2638 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 132:
#line 635 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2644 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 133:
#line 636 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2650 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 134:
#line 640 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2658 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 135:
#line 644 "includes/parse.y" /* yacc.c:1646  */
    {
        SuiteNode* snode = new SuiteNode((yyvsp[-1].vec));
        (yyval.node) = new IfNode((yyvsp[-3].node), snode);
        pool.add(snode);
        pool.add((yyval.node));
    }
#line 2669 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 136:
#line 653 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 2677 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 137:
#line 657 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 2683 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 161:
#line 708 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = new std::vector<Node*>();
        (yyval.vec)->push_back((yyvsp[0].node));
    }
#line 2692 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 162:
#line 713 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = (yyvsp[-1].vec);
    }
#line 2700 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 163:
#line 719 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = (yyvsp[-1].vec);
        (yyval.vec)->push_back((yyvsp[0].node));
    }
#line 2709 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 164:
#line 724 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = new std::vector<Node*>();
        (yyval.vec)->push_back((yyvsp[0].node));
    }
#line 2718 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 173:
#line 747 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[-1].node);
	}
#line 2726 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 174:
#line 750 "includes/parse.y" /* yacc.c:1646  */
    {}
#line 2732 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 177:
#line 758 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2740 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 178:
#line 761 "includes/parse.y" /* yacc.c:1646  */
    {}
#line 2746 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 179:
#line 765 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2754 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 180:
#line 768 "includes/parse.y" /* yacc.c:1646  */
    {}
#line 2760 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 181:
#line 771 "includes/parse.y" /* yacc.c:1646  */
    {}
#line 2766 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 182:
#line 773 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2774 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 183:
#line 779 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2782 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 184:
#line 782 "includes/parse.y" /* yacc.c:1646  */
    {
    
        Node* node = (yyvsp[-2].node);
        switch ( (yyvsp[-1].intNumber) ) {
           case LESS :    node = new LessBinaryNode((yyvsp[-2].node), (yyvsp[0].node)); break;
           case GREATER : node = new GreaterBinaryNode((yyvsp[-2].node), (yyvsp[0].node)); break;
           case EQEQUAL : node = new EqEqualBinaryNode((yyvsp[-2].node), (yyvsp[0].node)); break;
           case GREATEREQUAL : node = new GreaterEqualBinaryNode((yyvsp[-2].node), (yyvsp[0].node)); break;
           case LESSEQUAL :    node = new LessEqualBinaryNode((yyvsp[-2].node), (yyvsp[0].node)); break;
           // case GRLT :      node = new LessBinaryNode($1, $3); break;
           case NOTEQUAL :     node = new NotEqualBinaryNode((yyvsp[-2].node), (yyvsp[0].node)); break;
        }
        (yyval.node) = node;
        pool.add((yyval.node));
    }
#line 2802 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 185:
#line 799 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = LESS; }
#line 2808 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 186:
#line 800 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = GREATER; }
#line 2814 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 187:
#line 801 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = EQEQUAL; }
#line 2820 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 188:
#line 802 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = GREATEREQUAL; }
#line 2826 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 189:
#line 803 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = LESSEQUAL; }
#line 2832 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 190:
#line 804 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = GRLT; }
#line 2838 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 191:
#line 805 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = NOTEQUAL; }
#line 2844 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 192:
#line 806 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = IN; }
#line 2850 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 193:
#line 807 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = NOT_IN; }
#line 2856 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 194:
#line 808 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = IS; }
#line 2862 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 195:
#line 809 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.intNumber) = IS_NOT; }
#line 2868 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 196:
#line 813 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2876 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 197:
#line 817 "includes/parse.y" /* yacc.c:1646  */
    {
		// $1 | $3

		// check is int or not first

		// only for int
		(yyval.node) = new BarBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		pool.add((yyval.node));
	}
#line 2890 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 198:
#line 829 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2898 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 199:
#line 833 "includes/parse.y" /* yacc.c:1646  */
    {
		// $1 ^ $3
		// only for int
		(yyval.node) = new CircumflexBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		pool.add((yyval.node));
	}
#line 2909 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 200:
#line 842 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2917 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 201:
#line 846 "includes/parse.y" /* yacc.c:1646  */
    {
		// $1 & $3
		// check is int or not

		// only for int
		(yyval.node) = new AmpersandBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		pool.add((yyval.node));
	}
#line 2930 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 202:
#line 857 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 2938 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 203:
#line 861 "includes/parse.y" /* yacc.c:1646  */
    {
		/* check $1 and $3 is Int or not
		 * if not, throw an error
		 */

		// Node* double = new IntLiteral(2);

		/* << */
		if ((yyvsp[-1].intNumber) == 1)
		{
			(yyval.node) = new LeftShiftBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		}
		/* >> */
		else if((yyvsp[-1].intNumber) == 2)
		{
			(yyval.node) = new RightShiftBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		}
		pool.add((yyval.node));
	}
#line 2962 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 204:
#line 883 "includes/parse.y" /* yacc.c:1646  */
    {
		// <<
		(yyval.intNumber) = 1;
	}
#line 2971 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 205:
#line 888 "includes/parse.y" /* yacc.c:1646  */
    {
		// >>
		(yyval.intNumber) = 2;
	}
#line 2980 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 206:
#line 895 "includes/parse.y" /* yacc.c:1646  */
    {
		//1  return value
		(yyval.node) = (yyvsp[0].node);
	}
#line 2989 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 207:
#line 900 "includes/parse.y" /* yacc.c:1646  */
    {
		//2  return value with plus or minus
		/* Plus */
		if ((yyvsp[-1].intNumber) == 1)
		{
			(yyval.node) = new AddBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		}
		else if((yyvsp[-1].intNumber) == 2)
		{
			(yyval.node) = new SubBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		}

		pool.add((yyval.node));
	}
#line 3008 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 208:
#line 917 "includes/parse.y" /* yacc.c:1646  */
    {
		// 1 add
		(yyval.intNumber) = 1;
	}
#line 3017 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 209:
#line 922 "includes/parse.y" /* yacc.c:1646  */
    {
		// 2 sub
		(yyval.intNumber) = 2;
	}
#line 3026 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 210:
#line 929 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 3034 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 211:
#line 933 "includes/parse.y" /* yacc.c:1646  */
    {
		/* multiply */
		if ((yyvsp[-1].intNumber) == 1)
		{
			(yyval.node) = new MulBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		}
		/* division */
		else if ((yyvsp[-1].intNumber) == 2)
		{
			(yyval.node) = new DivBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		}
		/* Mod */
		else if ((yyvsp[-1].intNumber) == 3)
		{
			(yyval.node) = new PercentBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		}
		else
		{
			(yyval.node) = new FloorDivBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
		}

		pool.add((yyval.node));
	}
#line 3062 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 212:
#line 959 "includes/parse.y" /* yacc.c:1646  */
    {
		// 1 multiply *
	    (yyval.intNumber) = 1;
	}
#line 3071 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 213:
#line 964 "includes/parse.y" /* yacc.c:1646  */
    {
		// 2 division \/
		(yyval.intNumber) = 2;
	}
#line 3080 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 214:
#line 969 "includes/parse.y" /* yacc.c:1646  */
    {
		// 3 mod  \%
		(yyval.intNumber) = 3;
	}
#line 3089 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 215:
#line 974 "includes/parse.y" /* yacc.c:1646  */
    {
		// 4  \/\/ get the integer part of the division result
		(yyval.intNumber) = 4;
	}
#line 3098 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 216:
#line 981 "includes/parse.y" /* yacc.c:1646  */
    {
		/* plus */
		if ((yyvsp[-1].intNumber) == 1)
		{
			(yyval.node) = (yyvsp[0].node);
		}
		/* minus */
		else if ((yyvsp[-1].intNumber) == 2)
		{
			Node* zeroNode = new IntLiteral(0);
			(yyval.node) = new SubBinaryNode(zeroNode, (yyvsp[0].node));
			pool.add(zeroNode);
			pool.add((yyval.node));
		}
		/* tilde */
		else if ((yyvsp[-1].intNumber) == 3)
		{
			/* should check whether $2 is Int or not
			 * if not, throw an error.
			 */

			const IntLiteral* ptr = dynamic_cast<const IntLiteral*>((yyvsp[0].node)->eval());
			if (ptr != NULL)
			{
				Node* negOne = new IntLiteral(-1);
				(yyval.node) = new SubBinaryNode(negOne, (yyvsp[0].node));
				pool.add(negOne);
				pool.add((yyval.node));
			}
			else
			{
				throw std::string("Only can do this operations on INT number!");
			}

		}

	}
#line 3140 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 217:
#line 1019 "includes/parse.y" /* yacc.c:1646  */
    {
		 (yyval.node) = (yyvsp[0].node);
	}
#line 3148 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 218:
#line 1025 "includes/parse.y" /* yacc.c:1646  */
    { 
		(yyval.intNumber) = 1; 
	}
#line 3156 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 219:
#line 1029 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.intNumber) = 2;
	}
#line 3164 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 220:
#line 1033 "includes/parse.y" /* yacc.c:1646  */
    { 
		(yyval.intNumber) = 3;
	}
#line 3172 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 221:
#line 1039 "includes/parse.y" /* yacc.c:1646  */
    {
		// 2 ** 3
		(yyval.node) = new PowerBinaryNode((yyvsp[-3].node), (yyvsp[0].node));
		pool.add((yyval.node));
	}
#line 3182 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 222:
#line 1045 "includes/parse.y" /* yacc.c:1646  */
    {
        // function call position
        if ((yyvsp[0].vec) != NULL)
        {
            const IdentNode* id = dynamic_cast<IdentNode*>((yyvsp[-1].node));
            ParametersNode* pNode = new ParametersNode((yyvsp[0].vec));
            pool.add(pNode);
            (yyval.node) = new CallFunctionNode(id->getIdent(), pNode);
            pool.add((yyval.node));
        }
        else
        {
            (yyval.node) = (yyvsp[-1].node);
        }
	}
#line 3202 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 223:
#line 1063 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = (yyvsp[0].vec);
    }
#line 3210 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 224:
#line 1067 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.vec) = NULL; }
#line 3216 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 225:
#line 1071 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[-1].node);
	}
#line 3224 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 226:
#line 1075 "includes/parse.y" /* yacc.c:1646  */
    {
		throw std::string("unsupported operation");
	}
#line 3232 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 227:
#line 1079 "includes/parse.y" /* yacc.c:1646  */
    {
		throw std::string("unsupported operation");
	}
#line 3240 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 228:
#line 1083 "includes/parse.y" /* yacc.c:1646  */
    {
		throw std::string("unsupported operation");
	}
#line 3248 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 229:
#line 1087 "includes/parse.y" /* yacc.c:1646  */
    {
		// name
		// printf("%s\n", $1);
        if (!strcmp((yyvsp[0].id), "True")) 
        {
            (yyval.node) = new IntLiteral(1);
        }
        else if(!strcmp((yyvsp[0].id), "False"))
        {
            (yyval.node) = new IntLiteral(0);
        }
        else
        {
		    (yyval.node) = new IdentNode((yyvsp[0].id));
		}
        pool.add((yyval.node));
		delete[] (yyvsp[0].id);
	}
#line 3271 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 230:
#line 1106 "includes/parse.y" /* yacc.c:1646  */
    {
		// printf ("%f\n", $1);
		(yyval.node) = new FloatLiteral((yyvsp[0].fltNumber));
		pool.add((yyval.node));
	}
#line 3281 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 231:
#line 1112 "includes/parse.y" /* yacc.c:1646  */
    {
		// printf ("%d\n", $1);
		(yyval.node) = new IntLiteral((yyvsp[0].intNumber));
		pool.add((yyval.node));
	}
#line 3291 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 232:
#line 1118 "includes/parse.y" /* yacc.c:1646  */
    {
		throw std::string("unsupported operation");
	}
#line 3299 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 233:
#line 1124 "includes/parse.y" /* yacc.c:1646  */
    {
		throw std::string("unsupported operation");
	}
#line 3307 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 234:
#line 1128 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 3315 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 235:
#line 1134 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[0].node);
	}
#line 3323 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 236:
#line 1138 "includes/parse.y" /* yacc.c:1646  */
    {
		throw std::string("unsupported operation");
	}
#line 3331 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 245:
#line 1160 "includes/parse.y" /* yacc.c:1646  */
    {
		throw std::string("unsupported operation");
	}
#line 3339 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 246:
#line 1164 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[-2].node);
	}
#line 3347 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 249:
#line 1174 "includes/parse.y" /* yacc.c:1646  */
    {
        if ((yyvsp[-1].vec) == NULL)
        {
            (yyvsp[-1].vec) = new std::vector<Node*>();
        }
        (yyval.vec) = (yyvsp[-1].vec);
    }
#line 3359 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 250:
#line 1182 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.vec) = NULL;
    }
#line 3367 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 251:
#line 1186 "includes/parse.y" /* yacc.c:1646  */
    {
        // $$ = new IdentNode($2);
        // pool.add($$);
        delete[] (yyvsp[0].id); 
    }
#line 3377 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 269:
#line 1227 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[-2].node);
	}
#line 3385 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 270:
#line 1231 "includes/parse.y" /* yacc.c:1646  */
    {
		(yyval.node) = (yyvsp[-1].node);
	}
#line 3393 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 279:
#line 1252 "includes/parse.y" /* yacc.c:1646  */
    { delete [] (yyvsp[-5].id); }
#line 3399 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 280:
#line 1253 "includes/parse.y" /* yacc.c:1646  */
    { delete[] (yyvsp[-2].id);}
#line 3405 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 283:
#line 1261 "includes/parse.y" /* yacc.c:1646  */
    {
        if ((yyvsp[-1].vec) == NULL)
        {
            (yyvsp[-1].vec) = new std::vector<Node*>();
        }

        (yyvsp[-1].vec)->push_back((yyvsp[0].node));
        (yyval.vec) = (yyvsp[-1].vec);
    }
#line 3419 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 284:
#line 1273 "includes/parse.y" /* yacc.c:1646  */
    {
        if ((yyvsp[-2].vec) == NULL)
        {
            (yyvsp[-2].vec) = new std::vector<Node*>();
        }

        (yyvsp[-2].vec)->push_back((yyvsp[-1].node));
        (yyval.vec) = (yyvsp[-2].vec);
    }
#line 3433 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 285:
#line 1283 "includes/parse.y" /* yacc.c:1646  */
    { (yyval.vec) = NULL; }
#line 3439 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 290:
#line 1295 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[-1].node);
    }
#line 3447 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 291:
#line 1299 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 3455 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 292:
#line 1303 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = NULL;
    }
#line 3463 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 293:
#line 1309 "includes/parse.y" /* yacc.c:1646  */
    {
        (yyval.node) = (yyvsp[-1].node);
    }
#line 3471 "parse.tab.c" /* yacc.c:1646  */
    break;

  case 294:
#line 1313 "includes/parse.y" /* yacc.c:1646  */
    {
        // update symbol table
        // find the Name of $1, and update it with the value of $3
        (yyval.node) = new AsgBinaryNode((yyvsp[-2].node), (yyvsp[0].node));
        pool.add((yyval.node));
    }
#line 3482 "parse.tab.c" /* yacc.c:1646  */
    break;


#line 3486 "parse.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1361 "includes/parse.y" /* yacc.c:1906  */


#include <stdio.h>
void yyerror (const char *s)
{
    if(yylloc.first_line > 0)	{
        fprintf (stderr, "%d.%d-%d.%d:", yylloc.first_line, yylloc.first_column,
	                                     yylloc.last_line,  yylloc.last_column);
    }
    fprintf(stderr, " %s with [%s]\n", s, yytext);
}

