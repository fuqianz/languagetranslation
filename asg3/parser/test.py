#!/usr/bin/python

import os, sys
import fnmatch
import subprocess
import filecmp

def testCode( retcode, msg ):
  if retcode > 0:
    print msg
    sys.exit( 1 )
    
def loadDatafromFile( filePath ):
    f = open(filePath, 'r')
    
    data = f.readlines()
    return data
    
def fileCmp( filePath1, filePath2):
    data1 = loadDatafromFile(filePath1)
    data2 = loadDatafromFile(filePath2)
    
    flag = True
    pr = True
    
    for element in data1:
        if element not in data2:
            if pr :
                print "mypy"
                pr = False
            flag = False
            print element
            #break
    pr = True
    #print "mccabe"
    for element in data2:
        if element not in data1:
            if pr :
                print "mccabe"
                pr = False
            flag = False
            print element
            #break;
    return flag
testDir = os.path.join( os.getcwd(), 'cases')
if not os.path.isdir( testDir ):
  print testDir, "isn't a directory"
  sys.exit( 1 )

executable = os.path.join(os.getcwd(), "run")
if not os.path.isfile( executable ):
  retcode = subprocess.call("make",shell=True)
  testCode( retcode, "\tFAILED to make the scanner" )

noPassed = 0
noFailed = 0

files = os.listdir( testDir )
for x in files:
  if fnmatch.fnmatch(x, "*.py"):
    testcase = os.path.join(testDir, x)
    retcode = subprocess.call("./run < "+testcase+"> /tmp/out",shell=True)
    ret = subprocess.call("python -m mccabe " +testcase +" > /tmp/mccabe.out", shell = True)
    if retcode != 0:
      testCode( retcode, "\tFAILED to run test case "+x)
    else:
      if not fileCmp("/tmp/out", "/tmp/mccabe.out"): 
        noFailed += 1
        print "\tTEST CASE FAILED", x
      else :
        noPassed += 1
        print "testcase:", x, "passed"

print "\t", noPassed, "TEST CASE PASSED"
print "\t", noFailed, "TETS CASE FAILED"

