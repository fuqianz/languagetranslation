class Sample:
    def __enter__(self):
        print "In __enter__()"
        return "Foo"

    def __exit__(self, type, value, trace):
        print "In __exit__()"

            
def get_sample():
    return Sample()

def f() :
    with get_sample() as sample:
        print "sample:", sample
        if True:
            print "Helloworld"
