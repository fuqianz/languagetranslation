
def f():
    x = 5
    if x < 3:
        print x
    elif x > 5:
        print x + 5
    else:
        print x + 3

def g(n):
    x = 0
    if x < n:
        print x
    else:
        print x+1
    x += 1

def h():
    sum = 0
    if sum > -1 :
        sum += 10
    if sum < 15 :
        sum += 20    
    return sum

def k():
    sum = 0
    if sum > -1 :
        sum += 10
        if sum < 15 :
            sum += 20
        else :
            sum -= 20    
    return sum
