
# Function defined outside the class
def g(self, x, y):
    return min(x, x+y)

def h():
    sum = 0
    if sum > -1 :
        sum += 10
    if sum < 15 :
        sum += 20    
    def k():
        sum = 0
        if sum > -1 :
            sum += 10
    return sum
    
class C:

    def g(self):
        return 'hello world'

    h = g
def f(self, x, y):
    class C:
        f = f1

        def g(self):
            return 'hello world'

        h = g
    return min(x, x+y)

class Bag:
    def __init__(self):
        self.data = []
        
        
    class smallBag:

        def add(self, x):
            self.data.append(x)

        def addtwice(self, x):
            self.add(x)
            self.add(x)
            
    def addf(self, x):
            self.data.append(x)

class Mapping:
    def __init__(self, iterable):
        self.items_list = []
        self.__update(iterable)

    def update(self, iterable):
        for item in iterable:
            self.items_list.append(item)

    __update = update   # private copy of original update() method

class MappingSubclass(Mapping):

    def update(self, keys, values):
        # provides new signature for update()
        # but does not break __init__()
        for item in zip(keys, values):
            self.items_list.append(item)
