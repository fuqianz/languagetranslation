
def f():
    x = 5
    assert x > 5, "x is less than 6"
    x = 6
    print x

def g(n):
    x = 5
    
    if n > 4 :
        assert x > n, "x is less than n + 1"
    x += n
    print x
f()
g(3)
