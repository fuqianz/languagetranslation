
def c() :
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
    except IOError:
        print "Error: The file is not exist or open error"
    fh.close()
    
def d() :
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
        if True :
            print "OK!"
    except IOError:
        print "Error: The file is not exist or open error"
    fh.close()

def e() :
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
    except IOError:
        print "Error: The file is not exist or open error"
    else:
        print "write success"
    fh.close()
    
def f() : 
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
    finally:
        print "Error: The file is not exist or open error"

def g() : 
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
    finally:
        if True :
            print "Error: The file is not exist or open error"

def h() : 
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
        if True :
            print "OK!"
    finally:
        print "Error: The file is not exist or open error"

def i() :
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
    except IOError:
        print "Error: The file is not exist or open error"
    else:
        print "write success"
    finally:
        fh.close()
def j() :
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
    except IOError:
        print "Error: The file is not exist or open error"
    else:
        print "write success"
    fh.close()
    
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
    except IOError:
        print "Error: The file is not exist or open error"
    else:
        print "write success"
    finally:
        fh.close()
        
def k() :
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
    except IOError:
        print "Error: The file is not exist or open error"
    finally:
        fh.close()
def l() :
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
        try:
            fh = open("testfile", "w")
            fh.write("This is a file for testing!!")
        except IOError:
            print "Error: The file is not exist or open error"
        fh.close()
    except IOError:
        print "Error: The file is not exist or open error"
    else:
        print "write success"
                
def m() :
    try:
        fh = open("testfile", "w")
        fh.write("This is a file for testing!!")
        try:
            fh = open("testfile", "w")
            fh.write("This is a file for testing!!")
        except IOError:
            print "Error: The file is not exist or open error"
        else:
            print "write success"
        fh.close()
    except IOError:
        print "Error: The file is not exist or open error"
    else:
        print "write success"
def n() :
    try:
        fh1 = open("testfile", "w")
        fh1.write("This is a file for testing!!")
        try:
            fh2 = open("testfile", "w")
            fh2.write("This is a file for testing!!")
        except IOError:
            print "Error: The file is not exist or open error"
        finally:
            fh2.close()
    except IOError:
        print "Error: The file is not exist or open error"
    else:
        print "write success"
                
def o() :
    try:
        fh1 = open("testfile1", "w")
        fh1.write("This is a file for testing!!")
        try:
            fh2 = open("testfile2", "w")
            fh2.write("This is a file for testing!!")
        except IOError:
            print "Error: The file is not exist or open error"
        else:
            print "write success"
        fh2.close()
    except IOError:
        print "Error: The file is not exist or open error"
    else:
        print "write success"
    finally:
        fh1.close()
def p():
    try:
        file = open('test.txt', 'rb')
    except EOFError as e:
        print("An EOF error occurred.")
        raise e
    except IOError as e:
        print("An error occurred.")
        raise e
def q():
    try:
        file = open('test.txt', 'rb')
    except EOFError as e:
        print("An EOF error occurred.")
        raise e
    except IOError as e:
        print("An error occurred.")
        raise e
    finally :
        file.close()
