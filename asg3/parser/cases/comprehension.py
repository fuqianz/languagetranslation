
#A list/set/dict comprehension of generator expression is not equivalent to a for loop in mccabe.

# for list
def f():
    data = [1, 2, 3, 4, 5]
    data.sort()
    print data
    squares = [x**2 for x in range(10)]
    print squares
    prime = [x for x in range(10) if x % 2 == 0]
    print prime
    data = [x**2 if x % 2 == 0 else x**3 for x in range(10)]
    print data
    
    Sum = sum([x**2 for x in range(20)])
    print Sum
    
    # generator expression of list
    Sum = sum(x**2 for x in range(20))
    print Sum

# for set
def g(n):
    nums = {n**2 for n in range(10)}
    print nums
    
    nums = set(n**2 for n in range(10))
    print nums
    
# for dict
def h():
    dics = {k: v**3 for (k, v) in zip(string.ascii_lowercase, range(26))}
    print dics
  
    dics = {c: 0 for c in string.ascii_lowercase}
    print dics

    return dics

f()
g(3)
h()
