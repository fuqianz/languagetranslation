
# and
def f():
    is_it_true = (3 * 4 > 10) and (5 + 5 >= 10)
    print is_it_true
    
    is_it_true = (3 * 4 > 10) and (5 + 5 >= 10) and (4 * 4 > 15)
    print is_it_true

# or
def g(n):
    is_it_true = (3 * 4 > 10) or (5 + 5 > 10)
    print is_it_true
    
    is_it_true = (3 * 4 < 10) or (5 + 5 > 10) or (4 * 4 > 15) or (5 + 4 > 10)
    print is_it_true

# and or
def h():
    is_it_true = (3 * 4 > 10) or (5 + 5 > 10) and (4 * 4 > 15) or (5 + 4 > 10)
    print is_it_true
    
    is_it_true = (2 * 6 <= 10) and (32 / 8 >= 4) or not (5 ** 2 < 25)
    print is_it_true

f()
g(3)
h()
