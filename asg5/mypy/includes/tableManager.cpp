#include "tableManager.h"

TableManager& TableManager::getInstance()
{
    static TableManager instance;
    return instance;
}

const Node* TableManager::getEntry(const std::string& name)
{
    for (int index = currentScope ; index >= 0; --index)
    {
        if (tables[index].checkName(name) == true) {
            return tables[index].getValue(name);
        }
    }
    throw std::string("undefined error ") + name + std::string(" is not found");
    return tables[currentScope].getValue(name);
}

void TableManager::insert(const std::string& name, const Node* node)
{
    if (currentScope + 1 > tables.size())
    {
        tables.push_back(SymbolTable());
    }
    tables[currentScope].setValue(name, node);
}

bool TableManager::checkName(const std::string& name) const
{
    return tables[currentScope].checkName(name);
}

void TableManager::pushScope()
{
    currentScope ++;
}

void TableManager::popScope()
{
    currentScope --;
}

