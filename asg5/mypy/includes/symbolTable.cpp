#include <map>
#include <algorithm>
#include "symbolTable.h"
#include "literal.h"

const Node* SymbolTable::getValue(const std::string& name) const 
{
  std::map<std::string, const Node*>::const_iterator it = 
    table.find(name);
  if ( it == table.end() ) throw name+std::string(" not found");
  return it->second;
}

void SymbolTable::setValue(const std::string& name,const Node* val) 
{ 
  //std::pair<std::string, const Literal*> temp(name, val);
  //table.insert(temp);
  table[name] = val;  
}

bool SymbolTable::checkName(const std::string& name) const
{
    if (table.find(name) != table.end())
    {
        return true;
    }
    return false;
}

