#include <iostream>
#include <typeinfo>
#include <sstream>
#include <cmath>
#include <iterator>
#include <cstdlib>
#include <iomanip>
#include "ast.h"
#include "tableManager.h"

const Literal* IdentNode::eval() const { 
  const Literal* val = TableManager::getInstance().getEntry(ident)->eval();
  return val;
}


AsgBinaryNode::AsgBinaryNode(Node* left, Node* right) : 
  BinaryNode(left, right) { 
}


const Literal* LessBinaryNode::eval() const {
 if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  //return (*x+*y);

  int val = CompLiterals::compare (x , y);
  
  if (val < 0) {
  
      return new IntLiteral(1);
  }

  return new IntLiteral(0);

}
const Literal* GreaterBinaryNode::eval() const {
 if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  //return (*x+*y);
 
  int val = CompLiterals::compare (x , y);
  
  if (val > 0) {
  
      return new IntLiteral(1);
  }

  return new IntLiteral(0);

}
const Literal* EqEqualBinaryNode::eval() const {
 if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  //return (*x+*y);
  int val = CompLiterals::compare (x , y);
  
  if (val == 0) {
  
      return new IntLiteral(1);
  }

  return new IntLiteral(0);

}
const Literal* GreaterEqualBinaryNode::eval() const {
 if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  //return (*x+*y);
  int val = CompLiterals::compare (x , y);
  
  if (val >= 0) {
  
      return new IntLiteral(1);
  }

  return new IntLiteral(0);

}
const Literal* LessEqualBinaryNode::eval() const {
 if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  int val = CompLiterals::compare (x , y);
  
  if (val <= 0) {
  
      return new IntLiteral(1);
  }
  return new IntLiteral(0);

}
const Literal* NotEqualBinaryNode::eval() const {
 if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  int val = CompLiterals::compare (x , y);
  
  if (val > 0 || val < 0) {
  
      return new IntLiteral(1);
  }
  return new IntLiteral(0);

}

const Literal* AsgBinaryNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const Literal* res = right->eval();
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  TableManager::getInstance().insert(n, res);
  return res;
}
const Literal* AddEqualNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  //return (*x+*y);
  TableManager::getInstance().insert(n, (*x).operator+(*y));
  return NULL;
}

const Literal* SubEqualNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n, (*x)-(*y));
  return NULL;
}

const Literal* MulEqualNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n, (*x)*(*y));
  return NULL;
}

const Literal* DivEqualNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n, (*x)/(*y));
  return NULL;
}

const Literal* BarEqualNode::eval() const {
  if (!left || !right) {
    throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n,(*x) | (*y));
  return NULL;
}

const Literal* CircumflexEqualNode::eval() const {
  if (!left || !right) {
    throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n, (*x) ^ (*y));
  return NULL;
}

const Literal* AmpersandEqualNode::eval() const {
  if (!left || !right)
  {
    throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n, (*x) & (*y));
  return NULL;
}

const Literal* LeftShiftEqualNode::eval() const {
  if (!left || !right)
  {
	  throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  TableManager::getInstance().insert(n, (*x) << (*y));
  return NULL;
}

const Literal* RightShiftEqualNode::eval() const {
  if (!left || !right)
  {
	  throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n, (*x) >> (*y));
  return NULL;
}

const Literal* PercentEqualNode::eval() const {
  if (!left || !right)
  {
	  throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n, (*x) % (*y));
  return NULL;
}

const Literal* FloorDivEqualNode::eval() const {
  if (!left || !right)
  {
      throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  TableManager::getInstance().insert(n, x->floorDiv(*y));
  return NULL;
}

const Literal* PowerEqualNode::eval() const {
  if (!left || !right)
  {
	  throw "error";
  }
  const std::string n = static_cast<IdentNode*>(left)->getIdent();
  if (TableManager::getInstance().checkName(n) == false) {
    throw std::string("undefined error ") + n + std::string(" is not found");
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  
  TableManager::getInstance().insert(n, x->powerMul(*y));

  return NULL;
}
const Literal* AddBinaryNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  //return (*x+*y);
  return (*x).operator+(*y);
}

const Literal* SubBinaryNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x)-(*y));
}

const Literal* MulBinaryNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x)*(*y));
}

const Literal* DivBinaryNode::eval() const { 
  if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x)/(*y));
}

const Literal* BarBinaryNode::eval() const {
  if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x) | (*y));
}

const Literal* CircumflexBinaryNode::eval() const {
  if (!left || !right) {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x) ^ (*y));
}

const Literal* AmpersandBinaryNode::eval() const {
  if (!left || !right)
  {
    throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x) & (*y));
}

const Literal* LeftShiftBinaryNode::eval() const {
  if (!left || !right)
  {
	  throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x) << (*y));
}

const Literal* RightShiftBinaryNode::eval() const {
  if (!left || !right)
  {
	  throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x) >> (*y));
}

const Literal* PercentBinaryNode::eval() const {
  if (!left || !right)
  {
	  throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return ((*x) % (*y));
}

const Literal* FloorDivBinaryNode::eval() const {
  if (!left || !right)
  {
      throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return x->floorDiv(*y);
}

const Literal* PowerBinaryNode::eval() const {
  if (!left || !right)
  {
	  throw "error";
  }
  const Literal* x = left->eval();
  const Literal* y = right->eval();
  return x->powerMul(*y);
}

const Literal* IfNode::eval() const {
    const IntLiteral* lit = dynamic_cast<const IntLiteral*>(test->eval());
    if (lit->getVal()) {
        suiteNode->eval();
    }

    return NULL;
}

const Literal* FuncNode::eval() const {
  TableManager::getInstance().insert(fname, this);
  return NULL;
}

void  PrintNode::print() const {
    std::vector<Node*>::iterator it = nodes->begin();
    while(it != nodes -> end()) {
        (*it)->eval()->print();
        it++;
    }
}

const Literal* PrintNode::eval() const {
    print();
    return NULL;
}

const Literal* SuiteNode::eval() const {
 
    // set NoneType into the return location of the funciton

    Node* node = new NoneLiteral();
    TableManager::getInstance().insert(std::string("//RETURN//"), node);
    PoolOfNodes::getInstance().add(node);

    for (auto& stmt : *nodes)
    {
        stmt->eval();
        // if return Node, then break the loop
        const ReturnNode* rNode = dynamic_cast<ReturnNode*>(stmt);
        if (rNode != NULL)
        {
            break;
        }
    }
    return TableManager::getInstance().getEntry(std::string("//RETURN//"))->eval();
}

const Literal* ParametersNode::eval() const {
    return NULL;
}

void ParametersNode::assignParameters(Node* node) const {
  const ParametersNode* ptr = dynamic_cast<ParametersNode*>(node);

  if (ptr == NULL)
  {
      throw std::string("Function call error");
  }

  std::vector<Node*>* literalNodes = ptr->nodes;

  if (literalNodes->size() > nodes->size())
  {
      throw std::string("Function call error: too many arguments");
  }
  if (literalNodes->size() < nodes->size())
  {
      throw std::string("Function call error: provided arguments less than the number of expected arguments");
  }

  for (unsigned int index = 0 ; index < nodes->size(); ++index)
  {
      const IdentNode* idn = dynamic_cast<IdentNode*>(nodes->at(index));
      TableManager::getInstance().insert(idn->getIdent(), literalNodes->at(index));
  }
}

const Literal* ReturnNode::eval() const {
  TableManager::getInstance().insert(std::string("//RETURN//"), val);
  return val->eval();
}

const Literal* CallFunctionNode::runStmts(const FuncNode* funcNode) const {
    funcNode->paraNode->assignParameters(pnode);
    return funcNode->suiteNode->eval();
}

const Literal* CallFunctionNode::eval() const {
    const Node* node = TableManager::getInstance().getEntry(fname);
    const FuncNode* funcNode = dynamic_cast<const FuncNode*>(node);
    if (funcNode == NULL)
    {
        throw std::string("Function call error: is not a function");
    }
    TableManager::getInstance().pushScope();
    const Literal* rs = runStmts(funcNode);
    TableManager::getInstance().popScope();
    return rs;
}

