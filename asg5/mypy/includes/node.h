#pragma once
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <iomanip>

const double PRECISION = 0.0000000000000001;

class Literal;

class Node {
public:
  Node() {}
  virtual ~Node() {}
  virtual const Literal* eval() const = 0;
  virtual void print() const { 
    std::cout << "NODE" << std::endl; 
  }
};


