x = 2
def f() :
    def g() :
        print x
    g()
f()

x = 2
def f() :
    x = 3
    def g() :
        print x
    g()
f()
