%{
// output the mirror string

#include <iostream>
#include <map>
#include <stack>
#include <cstring>

/* store the upside-down character for certain character */
std::map<char, std::string> charToUpsideDownChar;

/* store the upside-down character for output reversely */
std::stack<std::string> upsideDownStack;

/* output all element in stack */
void output_stack(std::stack<std::string>& stack);

%}

letter      [a-zA-Z]
digit       [0-9]
blank       [ ]
newline     [\n]
otherchar   [&_?!"'`';\.\,\{\}<>\(\)\\\/]

allchar {letter}|{digit}|{blank}|{otherchar}

%%
{allchar}      {  
                   upsideDownStack.push(charToUpsideDownChar[*yytext]); 
               }
{newline}      {
                   output_stack(upsideDownStack);
               }
.              {
                   upsideDownStack.push(yytext);
               }
%%

/* release memory */
int yywrap() 
{
    charToUpsideDownChar.clear();
    yylex_destroy();
    return 1;
}

/* output all element in a stack */
void output_stack(std::stack<std::string>& stack)
{
    while (!stack.empty())
    {
        std::cout<<stack.top();
        stack.pop();
    }
    std::cout<<std::endl;
}
