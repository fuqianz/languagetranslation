#include <iostream>
#include <stdio.h>
#include <map>
#include <stack>

/* declare yylex() */
int yylex();

/* init the char to upside-down char map scanner */
void init_scanner(std::map<char, std::string>& charToStrMap);

extern FILE* yyin;
extern std::map<char, std::string> charToUpsideDownChar;


int main(int argc, char *argv[]) {
    
    // init scanner first
    init_scanner(charToUpsideDownChar);
    
    if(argc < 2)
    {
        // read input from stdin
        yylex();
    }
    else
    {
        // read input from a file
        FILE* inputFile = fopen(argv[1], "r");
    
        if (inputFile == NULL)
        {
            std::cout<<"Input file is not exist!"<<std::endl;
            return 0;
        }
    
        yyin = inputFile;
        yylex();
        fclose(inputFile);
    }
    
    std::cout << "Translation complete" << std::endl;

    return 0;
}

/* init char to upside-down char map */
void init_scanner(std::map<char, std::string>& charToStrMap)
{   
    charToStrMap['a'] = u8"\u0250";
    charToStrMap['b'] = u8"\u0071";
    charToStrMap['c'] = u8"\u0254";
    charToStrMap['d'] = u8"\u0070";
    charToStrMap['e'] = u8"\u01DD";
    charToStrMap['f'] = u8"\u025F";
    charToStrMap['g'] = u8"\u0253";
    charToStrMap['h'] = u8"\u0265";
    charToStrMap['i'] = u8"\u0131";
    charToStrMap['j'] = u8"\u027E";
    charToStrMap['k'] = u8"\u029E";
    charToStrMap['l'] = u8"\u006C";
    charToStrMap['m'] = u8"\u026F";
    charToStrMap['n'] = u8"\u0075";
    charToStrMap['o'] = u8"\u006F";
    charToStrMap['p'] = u8"\u0064";
    charToStrMap['q'] = u8"\u0062";
    charToStrMap['r'] = u8"\u0279";
    charToStrMap['s'] = u8"\u0073";
    charToStrMap['t'] = u8"\u0287";
    charToStrMap['u'] = u8"\u006E";
    charToStrMap['v'] = u8"\u028C";
    charToStrMap['w'] = u8"\u028D";
    charToStrMap['x'] = u8"\u0078";
    charToStrMap['y'] = u8"\u028E";
    charToStrMap['z'] = u8"\u007A";
    charToStrMap['A'] = u8"\u2200";
    charToStrMap['B'] = u8"\u0412";
    charToStrMap['C'] = u8"\u0186";
    charToStrMap['D'] = u8"\u15E1";
    charToStrMap['E'] = u8"\u018E";
    charToStrMap['F'] = u8"\u2132";
    charToStrMap['G'] = u8"\u2141";
    charToStrMap['H'] = u8"\u0048";
    charToStrMap['I'] = u8"\u0049";
    charToStrMap['J'] = u8"\u017F";
    charToStrMap['K'] = u8"\u22CA";
    charToStrMap['L'] = u8"\u2142";
    charToStrMap['M'] = u8"\u0057";
    charToStrMap['N'] = u8"\u004E";
    charToStrMap['O'] = u8"\u004F";
    charToStrMap['P'] = u8"\u0500";
    charToStrMap['Q'] = u8"\u038C";
    charToStrMap['R'] = u8"\u1D1A";
    charToStrMap['S'] = u8"\u0053";
    charToStrMap['T'] = u8"\u22A5";
    charToStrMap['U'] = u8"\u2229";
    charToStrMap['V'] = u8"\u039B";
    charToStrMap['W'] = u8"\u004D";
    charToStrMap['X'] = u8"\u0058";
    charToStrMap['Y'] = u8"\u2144";
    charToStrMap['Z'] = u8"\u005A";
    charToStrMap['1'] = u8"\u21C2";
    charToStrMap['2'] = u8"\u218A";
    charToStrMap['3'] = u8"\u218B";
    charToStrMap['4'] = u8"\u07C8";
    charToStrMap['5'] = u8"\u03DA";
    charToStrMap['6'] = u8"\u0039";
    charToStrMap['7'] = u8"\u3125";
    charToStrMap['8'] = u8"\u0038";
    charToStrMap['9'] = u8"\u0036";
    charToStrMap['0'] = u8"\u0030";
    charToStrMap['&'] = u8"\u214B";
    charToStrMap['_'] = u8"\u203E";
    charToStrMap['?'] = u8"\u00BF";
    charToStrMap['!'] = u8"\u00A1";
    charToStrMap['\"'] = u8"\u201E";
    charToStrMap['\''] = u8"\u002C";
    charToStrMap['.'] = u8"\u02D9";
    charToStrMap[','] = u8"\u0027";
    charToStrMap[';'] = u8"\u061B";
    charToStrMap[' '] = u8" ";
    charToStrMap['('] = u8")";
    charToStrMap[')'] = u8"(";
    charToStrMap['['] = u8"]";
    charToStrMap[']'] = u8"[";
    charToStrMap['{'] = u8"}";  
    charToStrMap['}'] = u8"{";
    charToStrMap['<'] = u8">";
    charToStrMap['>'] = u8"<";
    charToStrMap['/'] = u8"\\";
    charToStrMap['\\'] = u8"/";
}
